﻿using System.Collections.Generic;
using LiteNetLib.Utils;

namespace CB2.Networking
{
    public static class NetworkEventCode
    {
        public const byte InitialSync = 1;
        public const byte FrameUpdate = 2;
    }

    public static class NetworkRequestCode
    {
        public const byte Command = 1;
    }

    static class Extensions
    {
        public static void Put(this NetDataWriter writer, SimFrame v)
        {
            writer.Put(v.commands);
            writer.Put(v.hash);
        }

        public static void Get(this NetDataReader reader, ref SimFrame v)
        {
            reader.Get(v.commands);
            reader.Get(out v.hash);
        }

        public static void Put(this NetDataWriter writer, List<Command> list)
        {
            var count = list.Count;
            writer.Put((uint)count);
            for (int i = 0; i < count; ++i)
            {
                writer.Put(list[i]);
            }
        }

        public static void Get(this NetDataReader reader, List<Command> list)
        {
            var count = reader.GetUInt();
            for (int i = 0; i < count; ++i)
            {
                Command v;
                reader.Get(out v);
                list.Add(v);
            }
        }

        public static void Put(this NetDataWriter writer, Command v)
        {
            writer.Put(v.type);
            writer.Put(v.team);
            writer.Put(v.characterID);
            writer.Put(v.target);
            writer.Put(v.stance);
        }

        public static void Get(this NetDataReader reader, out Command v)
        {
            reader.Get(out v.type);
            reader.Get(out v.team);
            reader.Get(out v.characterID);
            reader.Get(out v.target);
            reader.Get(out v.stance);
        }

        public static void Put(this NetDataWriter writer, CommandType v)
        {
            writer.Put((byte)v);
        }

        public static void Get(this NetDataReader reader, out CommandType v)
        {
            v = (CommandType)reader.GetByte();
        }

        public static void Get(this NetDataReader reader, out byte v)
        {
            v = reader.GetByte();
        }

        public static void Put(this NetDataWriter writer, Simulation simulation)
        {
            var state = simulation.State;
            writer.Put(state.characters);
            writer.Put(state.projectiles);
            writer.Put(state.frameNumber);
            writer.Put(state.gameState);
            writer.Put(state.stateTimer);
            writer.Put(state.teams);
            writer.Put(state.nextCharacterID);
            writer.Put(state.nextProjectileID);
            writer.Put(state.random.state.w);
            writer.Put(state.random.state.z);
        }

        public static void Get(this NetDataReader reader, out Simulation simulation)
        {
            simulation = new Simulation();
            var state = simulation.State;
            reader.Get(state.characters);
            reader.Get(state.projectiles);
            reader.Get(out state.frameNumber);
            reader.Get(out state.gameState);
            reader.Get(out state.stateTimer);
            reader.Get(out state.teams);
            reader.Get(out state.nextCharacterID);
            reader.Get(out state.nextProjectileID);
            reader.Get(out state.random.state.w);
            reader.Get(out state.random.state.z);
            simulation.State = state;
        }

        public static void Put(this NetDataWriter writer, List<Character> list)
        {
            writer.Put((uint)list.Count);
            for (int i = 0; i < list.Count; ++i)
            {
                writer.Put(list[i]);
            }
        }

        public static void Get(this NetDataReader reader, List<Character> list)
        {
            list.Clear();
            var count = reader.GetUInt();
            for (int i = 0; i < count; ++i)
            {
                Character v;
                reader.Get(out v);
                list.Add(v);
            }
        }

        public static void Put(this NetDataWriter writer, List<Projectile> list)
        {
            writer.Put((uint)list.Count);
            for (int i = 0; i < list.Count; ++i)
            {
                writer.Put(list[i]);
            }
        }

        public static void Get(this NetDataReader reader, List<Projectile> list)
        {
            list.Clear();
            var count = reader.GetUInt();
            for (int i = 0; i < count; ++i)
            {
                Projectile v;
                reader.Get(out v);
                list.Add(v);
            }
        }

        public static void Put(this NetDataWriter writer, Team[] array)
        {
            writer.Put((uint)array.Length);
            for (int i = 0; i < array.Length; ++i)
            {
                writer.Put(array[i].money);
            }
        }

        public static void Get(this NetDataReader reader, out Team[] array)
        {
            var count = reader.GetUInt();
            array = new Team[count];
            for (int i = 0; i < count; ++i)
            {
                reader.Get(out array[i].money);
            }
        }

        public static void Put(this NetDataWriter writer, int[] array)
        {
            writer.Put((uint)array.Length);
            for (int i = 0; i < array.Length; ++i)
            {
                writer.Put(array[i]);
            }
        }

        public static void Get(this NetDataReader reader, out int[] array)
        {
            var count = reader.GetUInt();
            array = new int[count];
            for (int i = 0; i < count; ++i)
            {
                reader.Get(out array[i]);
            }
        }

        public static void Put(this NetDataWriter writer, Character v)
        {
            writer.Put(v.ID);
            writer.Put(v.state.attackTimer);
            writer.Put(v.state.velocity);
            writer.Put(v.state.position);
            writer.Put(v.state.balance);
            writer.Put(v.state.rotation);
            writer.Put(v.health);
            writer.Put(v.def);
            writer.Put(v.team);
            writer.Put(v.stance);
            writer.Put(v.targetCharacterID);
            writer.Put(v.moveTarget);
            writer.Put(v.moveToTarget);
            writer.Put(v.attackDamage);
            writer.Put(v.takeDamage);
            writer.Put(v.targetRotation);
            writer.Put(v.destroyed);
        }

        public static void Get(this NetDataReader reader, out Character v)
        {
            v = new Character();
            reader.Get(out v.ID);
            reader.Get(out v.state.attackTimer);
            reader.Get(out v.state.velocity);
            reader.Get(out v.state.position);
            reader.Get(out v.state.balance);
            reader.Get(out v.state.rotation);
            reader.Get(out v.health);
            reader.Get(out v.def);
            reader.Get(out v.team);
            reader.Get(out v.stance);
            reader.Get(out v.targetCharacterID);
            reader.Get(out v.moveTarget);
            reader.Get(out v.moveToTarget);
            reader.Get(out v.attackDamage);
            reader.Get(out v.takeDamage);
            reader.Get(out v.targetRotation);
            reader.Get(out v.destroyed);
        }

        public static void Put(this NetDataWriter writer, Projectile v)
        {
            writer.Put(v.ID);
            writer.Put(v.damage);
            writer.Put(v.origin);
            writer.Put(v.projectileDef);
            writer.Put(v.speed);
            writer.Put(v.time);
            writer.Put(v.targetCharacterID);
        }

        public static void Get(this NetDataReader reader, out Projectile v)
        {
            v = new Projectile();
            reader.Get(out v.ID);
            reader.Get(out v.damage);
            reader.Get(out v.origin);
            reader.Get(out v.projectileDef);
            reader.Get(out v.speed);
            reader.Get(out v.time);
            reader.Get(out v.targetCharacterID);
        }

        public static void Put(this NetDataWriter writer, CharacterDef def)
        {
            int id = Defs.GetCharacterDefID(def);
            writer.Put(id);
        }

        public static void Get(this NetDataReader reader, out CharacterDef def)
        {
            int id = reader.GetInt();
            def = Defs.GetCharacterDef(id);
        }

        public static void Put(this NetDataWriter writer, ProjectileDef def)
        {
            int id = Defs.GetProjectileDefID(def);
            writer.Put(id);
        }

        public static void Get(this NetDataReader reader, out ProjectileDef def)
        {
            int id = reader.GetInt();
            def = Defs.GetProjectileDef(id);
        }

        public static void Put(this NetDataWriter writer, Vector3 v)
        {
            writer.Put(v.x);
            writer.Put(v.y);
            writer.Put(v.z);
        }

        public static void Get(this NetDataReader reader, out Vector3 v)
        {
            v = new Vector3();
            reader.Get(out v.x);
            reader.Get(out v.y);
            reader.Get(out v.z);
        }

        public static void Put(this NetDataWriter writer, GameState v)
        {
            writer.Put((byte)v);
        }

        public static void Get(this NetDataReader reader, out GameState v)
        {
            v = (GameState)reader.GetByte();
        }

        public static void Put(this NetDataWriter writer, Stance v)
        {
            writer.Put((byte)v);
        }

        public static void Get(this NetDataReader reader, out Stance v)
        {
            v = (Stance)reader.GetByte();
        }

        public static void Put(this NetDataWriter writer, UnitID v)
        {
            writer.Put(v.value);
        }

        public static void Get(this NetDataReader reader, out UnitID v)
        {
            reader.Get(out v.value);
        }

        public static void Get(this NetDataReader reader, out float v)
        {
            v = reader.GetFloat();
        }

        public static void Get(this NetDataReader reader, out double v)
        {
            v = reader.GetDouble();
        }

        public static void Get(this NetDataReader reader, out ushort v)
        {
            v = reader.GetUShort();
        }

        public static void Get(this NetDataReader reader, out int v)
        {
            v = reader.GetInt();
        }

        public static void Get(this NetDataReader reader, out uint v)
        {
            v = reader.GetUInt();
        }

        public static void Get(this NetDataReader reader, out bool v)
        {
            v = reader.GetBool();
        }
    }
}

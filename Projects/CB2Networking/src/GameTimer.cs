﻿using System.Diagnostics;

namespace CB2.Networking
{
    internal class GameTimer
    {
        public void Start(double deltaTime)
        {
            m_deltaTime = deltaTime;
            m_stopWatch.Start();
        }

        public bool ShouldUpdate()
        {
            double elapsed = (double)m_stopWatch.ElapsedTicks / Stopwatch.Frequency;
            m_stopWatch.Reset();
            m_stopWatch.Start();
            m_accumulatedTime += elapsed;

            if (m_accumulatedTime >= m_deltaTime)
            {
                m_accumulatedTime -= m_deltaTime;
                return true;
            }

            return false;
        }

        private Stopwatch m_stopWatch = new Stopwatch();
        private double m_accumulatedTime;
        private double m_deltaTime;
    }
}

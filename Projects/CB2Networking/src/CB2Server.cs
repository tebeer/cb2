﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace CB2.Networking
{

    public class CB2Server : INetEventListener, INetLogger
    {
        internal const string ConnectionKey = "CB2";
        internal const int DefaultPort = 9050;

        public Action<string> Log;

        public void WriteNet(ConsoleColor color, string str, params object[] args)
        {
            if(Log != null)
                Log(string.Format(str, args));
        }

        public void RestartGame()
        {
            m_sim = new Simulation();
            m_frame.commands.Clear();

            var peers = m_net.GetPeers();
            for (int i = 0; i < peers.Length; ++i)
            {
                var peer = peers[i];
                SendInitialSync(peer, i);
            }
        }

        private void SendInitialSync(NetPeer peer, int id)
        {
            Log("SendInitialSync " + peer.EndPoint + " " + id);
            m_writer.Put(NetworkEventCode.InitialSync);
            m_writer.Put(m_sim);
            m_writer.Put(id);
            peer.Send(m_writer, SendOptions.ReliableOrdered);
            m_writer.Reset();
        }

        public CB2Server(Action<string> log)
        {
            Log = log;
            NetDebug.Logger = this;
            m_net = new NetManager(this, 10, ConnectionKey);

            m_net.Start(DefaultPort);
            m_writer = new NetDataWriter();

            m_sim = new Simulation();
            m_frame.commands = new List<Command>();

            m_timer = new GameTimer();
            m_timer.Start(Simulation.DeltaTime);
        }

        public void Update()
        {
            m_net.PollEvents();

            if (m_timer.ShouldUpdate())
            {
                //m_frame.currentTime = elapsed;

                m_sim.Update(m_frame);
                //Log(elapsed.ToString());

                //Log(m_sim.Characters.Count.ToString());
                m_writer.Put(m_sim);
                m_frame.hash = xxHashSharp.xxHash.CalculateHash(m_writer.Data, m_writer.Length);
                m_writer.Reset();

                m_writer.Put(NetworkEventCode.FrameUpdate);
                m_writer.Put(m_frame);

                m_net.SendToAll(m_writer, SendOptions.ReliableOrdered);
                m_writer.Reset();
                m_frame.commands.Clear();

            }
        }

        public void Destroy()
        {
            m_net.Stop();
        }

        //void OnPeerConnected(NetPeer peer);
        //void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo);
        //void OnNetworkError(NetEndPoint endPoint, int socketErrorCode);
        //void OnNetworkReceive(NetPeer peer, NetDataReader reader);
        //void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType);
        //void OnNetworkLatencyUpdate(NetPeer peer, int latency);

        void INetEventListener.OnNetworkError(NetEndPoint endPoint, int socketError)
        {
            Log("OnNetworkError");
        }
        void INetEventListener.OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            Log("OnNetworkLatencyUpdate");
        }
        void INetEventListener.OnNetworkReceive(NetPeer peer, NetDataReader reader)
        {
            Log("OnNetworkReceive");
            
            byte code = reader.GetByte();

            switch (code)
            {
                case NetworkRequestCode.Command:
                    reader.Get(m_frame.commands);
                    Log("Received Commands " + m_frame.commands.Count);
                    break;

                default:
                    Log("Unknown message: " + code);
                    break;
            }
        }
        void INetEventListener.OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            Log("OnNetworkReceiveUnconnected");
        }
        void INetEventListener.OnPeerConnected(NetPeer peer)
        {
            Log("OnPeerConnected");
            Log(string.Format("We got connection: {0}", peer.EndPoint));
            Log("Peer count: " + m_net.PeersCount);

            int playerId = m_net.PeersCount-1;

            SendInitialSync(peer, playerId);
        }
        void INetEventListener.OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Log("OnPeerDisconnected");
        }

        private NetManager m_net;
        private NetDataWriter m_writer;

        private Simulation m_sim;
        private SimFrame m_frame;
        private GameTimer m_timer;

    }
}
﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace CB2.Networking
{

    public class CB2Client : INetEventListener, INetLogger
    {
        public Action<string> Log;

        public Simulation Sim { get { return m_sim; } }
        public int OurTeam { get { return m_ourTeam; } }
        public bool Desync { get { return m_desync; } }

        public bool IsConnected { get { return m_serverPeer != null; } }

        public void WriteNet(ConsoleColor color, string str, params object[] args)
        {
            Log(string.Format(str, args));
        }

        public CB2Client()
        {
            NetDebug.Logger = this;
            m_net = new NetManager(this, CB2Server.ConnectionKey);

            m_writer = new NetDataWriter();

            m_frameCache = new Queue<SimFrame>();
            m_frameQueue = new Queue<SimFrame>();

            //m_timer = new GameTimer();
            //m_timer.Start(Simulation.DeltaTime);
        }

        public void Connect(string address, int port = CB2Server.DefaultPort)
        {
            m_net.Start();
            m_net.Connect(address, port);
        }

        public void SendCommands(List<Command> commands)
        {
            if (commands.Count == 0)
                return;

            if (m_serverPeer == null)
                return;

            for (int i = 0; i < commands.Count; ++i)
            {
                var cmd = commands[i];
                cmd.team = (byte)m_ourTeam;
                commands[i] = cmd;
            }

            m_writer.Put(NetworkRequestCode.Command);
            m_writer.Put(commands);
            m_serverPeer.Send(m_writer, SendOptions.ReliableOrdered);
            m_writer.Reset();
        }

	    public void Update()
        {
            m_net.PollEvents();

            if (m_sim != null)
            {
                if (m_frameQueue.Count > 0)
                {
                    var frame = m_frameQueue.Dequeue();

                    if (frame.commands.Count != 0)
                        Log("Commands: " + frame.commands.Count);

                    m_sim.Update(frame);

                    m_writer.Put(m_sim);
                    uint ourHash = xxHashSharp.xxHash.CalculateHash(m_writer.Data, m_writer.Length);
                    m_writer.Reset();

                    if (frame.hash != ourHash)
                    {
                        Log(string.Format("Desync detected, server: {0} != client: {1}", frame.hash, ourHash));
                        m_desync = true;
                    }

                    frame.commands.Clear();
                    m_frameCache.Enqueue(frame);
                }
            } 
	    }

        public void Destroy()
        {
            m_net.Stop();
        }

        void INetEventListener.OnNetworkError(NetEndPoint endPoint, int socketError)
        {
            Log("OnNetworkError");
        }
        void INetEventListener.OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            Log("OnNetworkLatencyUpdate");
        }
        void INetEventListener.OnNetworkReceive(NetPeer peer, NetDataReader reader)
        {
            //Log("OnNetworkReceive");

            byte code = reader.GetByte();

            switch (code)
            {
                case NetworkEventCode.InitialSync:
                    Log("Received initial sync");
                    reader.Get(out m_sim);
                    reader.Get(out m_ourTeam);
                    m_desync = false;
                    break;

                case NetworkEventCode.FrameUpdate:

                    SimFrame frame;
                    if (m_frameCache.Count == 0)
                        frame = new SimFrame() { commands = new List<Command>() };
                    else
                        frame = m_frameCache.Dequeue();

                    reader.Get(ref frame);
                    m_frameQueue.Enqueue(frame);
                    break;

                default:
                    Log("Unknown message: " + code);
                    break;
            }
        }

        void INetEventListener.OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
        {
            Log("OnNetworkReceiveUnconnected");
        }
        void INetEventListener.OnPeerConnected(NetPeer peer)
        {
            Log("OnPeerConnected");
            m_serverPeer = peer;
        }
        void INetEventListener.OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            Log("OnPeerDisconnected");
            if (m_serverPeer == peer)
                m_serverPeer = null;
        }

        private NetManager m_net;
        private NetDataWriter m_writer;
        private NetPeer m_serverPeer;

        private Simulation m_sim;
        private Queue<SimFrame> m_frameCache;
        private Queue<SimFrame> m_frameQueue;
        //private GameTimer m_timer;

        private bool m_desync;
        private int m_ourTeam;
    }
}

﻿using CB2.Networking;
using System.Threading;
using System;

class Program
{
    static void Main(string[] args)
    {
        var server = new CB2Server((str) => Console.WriteLine(str));

        bool run = true;
        while (run)
        {
            if (Console.KeyAvailable)
            {
                var key = Console.ReadKey();
                if (key.KeyChar == 'q')
                    run = false;
                else if (key.KeyChar == 'r')
                    server.RestartGame();
            }

            server.Update();

            Thread.Sleep(15);
        }
        server.Destroy();
    }
}


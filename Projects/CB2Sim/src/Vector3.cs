﻿namespace CB2
{
    public struct Vector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static float Dot(Vector3 v1, Vector3 v2)
        {
            return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
        }

        public static Vector3 Lerp(Vector3 v1, Vector3 v2, float t)
        {
            v1.x = Math.Lerp(v1.x, v2.x, t);
            v1.y = Math.Lerp(v1.y, v2.y, t);
            v1.z = Math.Lerp(v1.z, v2.z, t);
            return v1;
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
        }

        public static Vector3 operator *(Vector3 v1, float f)
        {
            return new Vector3(v1.x * f, v1.y * f, v1.z * f);
        }

        public static Vector3 operator *(float f, Vector3 v1)
        {
            return new Vector3(v1.x * f, v1.y * f, v1.z * f);
        }

        public static Vector3 operator /(Vector3 v1, float f)
        {
            return new Vector3(v1.x / f, v1.y / f, v1.z / f);
        }

        public float magnitude
        {
            get
            {
                return (float)System.Math.Sqrt(x * x + y * y + z * z);
            }
        }

        public Vector3 normalized
        {
            get
            {
                float m = magnitude;
                if (m == 0)
                    return new Vector3(1, 0, 0);
                return new Vector3(x / m, y / m, z / m);
            }
        }

        public static Vector3 zero
        {
            get
            {
                return new Vector3(0, 0, 0);
            }
        }

        public static Vector3 ClampMagnitude(Vector3 v, float magnitude)
        {
            float m = v.magnitude;
            if (m > magnitude)
            {
                m = 1.0f / m * magnitude;
                v *= m;
            }
            return v;
        }
    }
}

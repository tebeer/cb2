﻿using System.Collections.Generic;
using System.Globalization;

namespace CB2
{
    public enum CommandType : byte
    {
        Move,
        Purchase,
        SetStance,
    }

    public enum GameState : byte
    {
        Start,
        Running,
    }

    public struct Command
    {
        public CommandType type;
        public byte team;
        public UnitID characterID;
        public Vector3 target;
        public Stance stance;
    }

    public struct SimFrame
    {
        public List<Command> commands;
        public uint hash;
    }

    public class Simulation
    {
        public const float DeltaTime = 1.0f / 20.0f;

        public List<Character> Characters { get { return m_state.characters; } }
        public List<Projectile> Projectiles { get { return m_state.projectiles; } }
        public SimulationState State { get { return m_state; } set { m_state = value; } }

        private float RandomValue
        {
            get
            {
                return m_state.random.GetFloat();
            }
        }

        public Simulation()
        {
            m_state = new SimulationState();
            m_state.characters = new List<Character>();
            m_state.projectiles = new List<Projectile>();
            m_state.spawns = new List<CharacterSpawn>();
            m_state.nextCharacterID.Next();
            m_state.nextProjectileID.Next();
            m_state.random = new RNG();
            m_state.teams = new Team[2];
            m_state.map = CreateMap(Defs.TestMap);

            //for (int i = 0; i < 1; ++i)
            //{
            //    CreateCharacter(Defs.Human_LightInfantry, 1, new Vector3(-10, 0, 10 * (RandomValue - .5f)), Stance.HoldPosition);
            //}
            //for (int i = 0; i < 1; ++i)
            //{
            //    CreateCharacter(Defs.Skeleton, 2, new Vector3(10, 0, 10 * (RandomValue - .5f)), Stance.Roam);
            //}
            //CreateCharacter(Defs.Golem, 2, new Vector3( 10, 0, 10 * (Random.value - .5f)), Stance.Roam);
            for (int i = 0; i < m_state.teams.Length; ++i)
                m_state.teams[i].money = 0;

            Reset();
        }

        public void Update(SimFrame frame)
        {
            m_state.frameNumber++;
            ApplyCommands(frame.commands);
            UpdateInternal();
        }

        private void ChangeState(GameState state, float time)
        {
            m_state.gameState = state;
            m_state.stateTimer = time;
        }

        private void UpdateInternal()
        {
            m_state.stateTimer -= DeltaTime;
            if (m_state.stateTimer <= 0)
            {
                switch (m_state.gameState)
                {
                    case GameState.Start:
                        Start();
                        break;
                    case GameState.Running:
                        RoundEnd();
                        break;
                }
            }

            if (m_state.gameState == GameState.Running)
            {
                UpdateTargetCounts();

                for (int i = 0; i < m_state.characters.Count; ++i)
                {
                    var character = m_state.characters[i];
                    UpdateCharacter(ref character);
                    m_state.characters[i] = character;
                }

                for (int i = m_state.projectiles.Count - 1; i >= 0; --i)
                {
                    var projectile = m_state.projectiles[i];

                    projectile.time += projectile.speed * DeltaTime;
                    if (projectile.time >= 1.0f)
                    {
                        DealDamage(projectile.targetCharacterID, projectile.damage);
                        m_state.projectiles.RemoveAt(i);
                    }
                }

                for (int i = m_state.characters.Count - 1; i >= 0; --i)
                {
                    if (m_state.characters[i].health <= 0)
                    {
                        m_state.characters[i].destroyed = true;
                        m_state.characters.RemoveAt(i);
                    }
                }

                for (int i = 0; i < m_teamCharCount.Length; ++i)
                    m_teamCharCount[i] = 0;

                for (int i = 0; i < m_state.characters.Count; ++i)
                {
                    var c1 = m_state.characters[i];
                    m_teamCharCount[c1.team]++;

                    for (int j = i + 1; j < m_state.characters.Count; ++j)
                    {
                        var c2 = m_state.characters[j];
                        DetectCollision(ref c1, ref c2);
                        m_state.characters[i] = c1;
                        m_state.characters[j] = c2;
                    }
                }


                if (m_state.gameState == GameState.Running)
                {
                    UpdateCapturePoints();
                    CheckGameEnd();
                }
            }
        }

        private void UpdateCapturePoints()
        {
            for (int i = 0; i < m_state.map.capturePoints.Length; ++i)
            {
                var point = m_state.map.capturePoints[i];
                if (!point.active)
                    continue;

                for (int j = 0; j < m_state.characters.Count; ++j)
                {
                    var character = m_state.characters[j];

                    float distance = (point.def.position - character.state.position).magnitude;

                    if (distance <= point.def.radius)
                    {
                        for (int k = 0; k < point.capture.Length; ++k)
                        {
                            if (k == character.team)
                            {
                                point.capture[k] += DeltaTime;
                            }
                            else
                            {
                                point.capture[k] -= 2 * DeltaTime;
                                if (point.capture[k] < 0)
                                    point.capture[k] = 0;
                            }
                        }
                    }
                }
            }
        }

        private Vector3 GetSpawnPoint(int team)
        {
            var spawns = m_state.map.def.spawnPoints;
            for (int i = 0; i < spawns.Length; ++i)
            {
                if (spawns[i].team == team)
                {
                    var pos = spawns[i].position;
                    pos.x += 10 * (RandomValue - .5f);
                    pos.z += 10 * (RandomValue - .5f);
                    return pos;
                }
            }

            throw new System.Exception("Spawnpoint not found for team: " + team);
        }

        private CharacterDef GetRandomDef(float randomvalue, int team)
        {
            var defs = Defs.PurchasableDefs[team];
            return defs[(int)(randomvalue * defs.Length)];
        }

        private void CheckGameEnd()
        {
            bool gameEnd = false;

            int aliveTeamCount = 0;
            int winnerTeam = 0;
            for (int i = 0; i < m_teamCharCount.Length; ++i)
            {
                if (m_teamCharCount[i] != 0)
                {
                    aliveTeamCount++;
                    winnerTeam = i;
                }
            }

            if (aliveTeamCount <= 1)
            {
                gameEnd = true;
            }

            //for (int i = 0; i < m_state.map.capturePoints.Length; ++i)
            //{
            //    var point = m_state.map.capturePoints[i];
            //    if (!point.active)
            //        continue;
            //
            //    for (int j = 0; j < point.capture.Length; ++j)
            //    {
            //        if (point.capture[j] >= point.def.captureTime - .01f)
            //        {
            //            if (point.def.type == CapturePointType.Primary)
            //            {
            //                //m_state.teamMoney[j] += 1000;
            //                gameEnd = true;
            //            }
            //            else if (point.def.type == CapturePointType.Secondary)
            //            {
            //                m_state.teamMoney[j] += 400;
            //                point.active = false;
            //            }
            //        }
            //    }
            //}

            if (gameEnd)
            {
                Reset();
            }
        }

        private void RoundEnd()
        {
            //for (int i = 0; i < m_state.map.capturePoints.Length; ++i)
            //{
            //    var point = m_state.map.capturePoints[i];
            //    if (!point.active)
            //        continue;
            //
            //    for (int j = 0; j < point.capture.Length; ++j)
            //    {
            //        if (point.capture[j] >= point.def.captureTime - .01f)
            //        {
            //            if (point.def.type == CapturePointType.Primary)
            //            {
            //                //m_state.teamMoney[j] += 1000;
            //            }
            //            else if (point.def.type == CapturePointType.Secondary)
            //            {
            //                m_state.teamMoney[j] += 400;
            //                point.active = false;
            //            }
            //        }
            //    }
            //}
            Reset();
        }

        private void Reset()
        {
            //for (int i = 0; i < m_state.characters.Count; ++i)
            //{
            //    var character = m_state.characters[i];
            //    m_state.teams[character.team].money -= character.def.price / 2;
            //}

            for (int i = 0; i < m_state.characters.Count; ++i)
            {
                m_state.characters[i].destroyed = true;
            }
            m_state.characters.Clear();

            for (int i = 0; i < m_state.spawns.Count; ++i)
            {
                var spawn = m_state.spawns[i];
                CreateCharacter(spawn.def, spawn.team, Stance.Roam);
            }

            for (int i = 0; i < m_state.teams.Length; ++i)
            {
                m_state.teams[i].money += 1000;
            }
            
            for (int i = 0; i < m_state.map.capturePoints.Length; ++i)
            {
                var point = m_state.map.capturePoints[i];
                point.active = true;
                for(int j = 0; j < point.capture.Length; ++j)
                    point.capture[j] = 0;
            }

            for (int i = 0; i < m_state.characters.Count; ++i)
            {
                var c = m_state.characters[i];
                c.state.position = GetSpawnPoint(c.team);
                c.state.balance = c.state.position;
                c.stance = Stance.Roam;
            }

            bool run = true;
            while(run)
            {
                var rand = RandomValue;
                run = false;
                run |= PurchaseCharacter(GetRandomDef(rand, 1), 1);
            }

            ChangeState(GameState.Start, 10);
        }

        private void Start()
        {
            ChangeState(GameState.Running, 90);

            for (int i = 0; i < m_state.characters.Count; ++i)
            {
                //m_state.characters[i].stance = Stance.Roam;
            }
        }

        private void ApplyCommands(List<Command> commands)
        {
            for (int i = 0; i < commands.Count; ++i)
            {
                var cmd = commands[i];

                switch(cmd.type)
                {
                    case CommandType.Move:
                        {
                            var character = TryGetCharacter(cmd.characterID);
                            if (character == null)
                                break;

                            character.moveTarget = cmd.target;
                            character.moveToTarget = true;
                            character.stance = Stance.HoldPosition;
                            character.targetCharacterID.Reset();
                            break;
                        }
                    case CommandType.Purchase:
                        {
                            var def = Defs.GetCharacterDef((int)cmd.characterID.value);

                            PurchaseCharacter(def, cmd.team);

                            break;
                        }
                    case CommandType.SetStance:
                        {
                            var character = TryGetCharacter(cmd.characterID);
                            if (character == null)
                                break;

                            character.stance = cmd.stance;
                            character.moveToTarget = false;

                        break;
                        }
                    default:
                        break;
                }
            }
        }

        private bool PurchaseCharacter(CharacterDef def, int team)
        {
            if (m_state.teams[team].money >= def.price)
            {
                m_state.spawns.Add(new CharacterSpawn { team = team, def = def });
                m_state.teams[team].money -= def.price;
                CreateCharacter(def, team, Stance.Roam);
                return true;
            }

            //if (m_state.teams[team].money >= def.price)
            //{
            //    m_state.teams[team].money -= def.price;
            //    CreateCharacter(def, team, Stance.HoldPosition);
            //    return true;
            //}
            return false;
        }

        private void CreateCharacter(CharacterDef def, int team, Stance stance)
        {
            var spawnPoint = GetSpawnPoint(team);
            for (int i = 0; i < def.units; ++i)
            {
                Character character = new Character();

                character.team = team;
                character.state.position = spawnPoint + new Vector3((-def.units/2 + i) * def.radius * 2, 0, 0);
                character.state.balance = character.state.position;

                character.health = def.maxHealth;
                character.def = def;

                character.stance = stance;
                character.ID = m_state.nextCharacterID.Next();

                m_state.characters.Add(character);
            }
        }

        private Map CreateMap(MapDef def)
        {
            var map = new Map();
            map.def = def;
            map.capturePoints = new CapturePoint[def.capturePoints.Length];
            for (int i = 0; i < map.capturePoints.Length; ++i)
            {
                map.capturePoints[i] = new CapturePoint();
                map.capturePoints[i].def = def.capturePoints[i];
                map.capturePoints[i].capture = new float[3];
            }
            return map;
        }

        private void DetectCollision(ref Character c1, ref Character c2)
        {
            Vector3 diff = c2.state.position - c1.state.position;

            float dist = diff.magnitude;
            float r = c1.def.radius + c2.def.radius;
            if (dist < r)
            {
                var dir = diff.normalized;
                float over = r - dist;

                float f1 = c1.def.radius / r;
                c1.state.position -= (1.0f - f1) * over * dir;
                c2.state.position += f1 * over * dir;
                if (c1.team != c2.team)
                {
                    //if (c1.moveToTarget || c1.targetCharacterID == 0)
                    {
                        //c1.moveToTarget = false;
                        c1.targetCharacterID = c2.ID;
                    }
                    //if (c2.moveToTarget || c2.targetCharacterID == 0)
                    {
                        //c2.moveToTarget = false;
                        c2.targetCharacterID = c1.ID;
                    }
                }
            }
        }

        private void DealDamage(UnitID characterID, float damage)
        {
            var character = TryGetCharacter(characterID);
            if (character == null)
                return;

            DealDamage(character, damage);
        }

        private void DealDamage(Character character, float damage)
        {
            character.health -= damage;

            //if (character.state.attackTimer > character.def.weapon.anims.hitTime)
            //{
            //    character.state.attackTimer += (1.0f - character.state.attackTimer) * character.def.weapon.damageInterrupt;
            //}

            if (RandomValue > .5f)
            {
                if (character.state.attackTimer > .5f * (1.0f + character.def.weapon.anims.hitTime)
                 || character.state.attackTimer < character.def.weapon.anims.hitTime)
                {
                    character.takeDamage = 1.0f;
                    character.state.attackTimer = 0;
                }
            }
        }

        private void UpdateCharacter(ref Character character)
        {
            Vector3 targetVelocity = Vector3.zero;

            if (character.takeDamage > 0.0f)
            {
                character.takeDamage -= 2.0f * DeltaTime;
            }
            else if (character.state.attackTimer > 0.0f)
            {
                var weapon = character.def.weapon;

                character.state.attackTimer -= weapon.speed * DeltaTime * GetHPFactor(character);

                var target = TryGetCharacter(character.targetCharacterID);

                if (target != null)
                {
                    Vector3 toTarget = target.state.position - character.state.position;
                    float distToTarget = toTarget.magnitude;
                    character.targetRotation = Math.GetDir(toTarget.x, toTarget.z) + 90;

                    Vector3 dirToTarget = toTarget / distToTarget;
                    target.state.threat += dirToTarget * (1.0f - .5f * distToTarget / weapon.range);

                    if (character.state.attackTimer <= weapon.anims.hitTime && character.attackDamage == true)
                    {
                        character.attackDamage = false;

                        if (character.def.weapon.weaponType == WeaponType.Melee)
                        {
                            DealDamage(target, weapon.damage);
                            target.targetCharacterID = character.ID;
                            //target.moveToTarget = false;
                        }
                        else
                        {
                            var projectile = new Projectile();
                            projectile.projectileDef = weapon.projectile;

                            projectile.targetCharacterID = target.ID;
                            projectile.damage = weapon.damage;
                            projectile.speed = weapon.projectile.speed / distToTarget;
                            projectile.origin = character.state.position;
                            projectile.ID = m_state.nextProjectileID.Next();
                            m_state.projectiles.Add(projectile);
                        }
                    }
                }
            }
            else
            {
                if (character.moveToTarget)
                {
                    Vector3 toTarget = character.moveTarget - character.state.position;
                    float dToTarget = toTarget.magnitude;
                    
                    character.targetRotation = Math.GetDir(toTarget.x, toTarget.z) + 90;
                    if (dToTarget > .2f)
                    {
                        targetVelocity = Vector3.ClampMagnitude(toTarget * 4, GetMaxMoveSpeed(character));
                    }
                    else
                    {
                        character.moveToTarget = false;
                    }
                }
                else
                {
                    //var threat = character.state.threat;
                    //character.state.threat = Vector3.zero;
                    //if (threat.magnitude > 1.0f)
                    //{
                    //    // Move backwards
                    //    targetVelocity = Vector3.ClampMagnitude(threat, .5f*GetMaxMoveSpeed(character));
                    //    character.targetRotation = Math.GetDir(targetVelocity.x, targetVelocity.z) - 90;
                    //}
                    //else
                    {
                        var target = TryGetCharacter(character.targetCharacterID);

                        if (target == null)
                            target = FindTarget(character.state.position, character.team, 100.0f);

                        if (target != null)
                        {
                            character.targetCharacterID = target.ID;

                            Vector3 toTarget = target.state.position - character.state.position;
                            float dToTarget = toTarget.magnitude;

                            character.targetRotation = Math.GetDir(toTarget.x, toTarget.z) + 90;

                            if (dToTarget > character.def.weapon.range)
                            {
                                if (character.stance == Stance.Roam)
                                {
                                    targetVelocity = Vector3.ClampMagnitude(toTarget, GetMaxMoveSpeed(character));
                                }
                            }
                            else
                            {
                                if (character.state.attackTimer <= 0.0f)
                                {
                                    character.state.attackTimer = 1.0f;
                                    character.attackDamage = true;
                                }
                            }
                        }
                        else
                            character.targetCharacterID = UnitID.Null;
                    }
                }
            }


            float dot = Vector3.Dot(character.state.velocity, targetVelocity);
            float acc = 1.0f;
            if (dot <= 0.1f)
                acc *= 2;

            character.state.balance = Vector3.Lerp(character.state.balance, character.state.position + targetVelocity - character.state.velocity, 5 * DeltaTime);
            character.state.velocity += (character.state.balance - character.state.position) * 5 * DeltaTime;
            character.state.rotation = Math.MoveTowardsAngle(character.state.rotation, character.targetRotation, 360.0f * DeltaTime);

            character.state.position += character.state.velocity * DeltaTime;
            character.state.position.y = 0;
        }

        private float GetMaxMoveSpeed(Character character)
        {
            return character.def.moveSpd * GetHPFactor(character);
        }

        private float GetHPFactor(Character character)
        {
            return .5f + .5f * character.health / character.def.maxHealth;
        }

        private void UpdateTargetCounts()
        {
            // Count how many times each character is targeted
            m_targetData.Clear();
            for (int i = 0; i < m_state.characters.Count; ++i)
            {
                var character = m_state.characters[i];
                if (!character.targetCharacterID.IsValid)
                    continue;

                bool addNew = true;
                for (int j = 0; j < m_targetData.Count; ++j)
                {
                    var t = m_targetData[j];
                    if (t.characterID == character.targetCharacterID)
                    {
                        t.count++;
                        m_targetData[j] = t;
                        addNew = false;
                    }
                }
                if (addNew)
                {
                    m_targetData.Add(new TargetSearchData() { characterID = character.targetCharacterID, count = 1 });
                }
            }
        }

        private uint GetTargetedCount(UnitID characterID)
        {
            for (int i = 0; i < m_targetData.Count; ++i)
                if (m_targetData[i].characterID == characterID)
                    return m_targetData[i].count;
            return 0;
        }

        private Character FindTarget(Vector3 pos, int team, float maxDist)
        {
            float minDist = maxDist;
            Character target = null;
            for (int i = 0; i < m_state.characters.Count; ++i)
            {
                if (m_state.characters[i].team == team)
                    continue;

                if (GetTargetedCount(m_state.characters[i].ID) > 3)
                    continue;

                Vector3 diff = m_state.characters[i].state.position - pos;
                float dist = diff.magnitude;
                if (dist < minDist)
                {
                    minDist = dist;
                    target = m_state.characters[i];
                }
            }

            return target;
        }

        private struct TargetSearchData
        {
            public UnitID characterID;
            public uint count;
        }

        private List<TargetSearchData> m_targetData = new List<TargetSearchData>();
        private int[] m_teamCharCount = new int[10];
        private SimulationState m_state;

        public Character TryGetCharacter(UnitID characterID)
        {
            if (!characterID.IsValid)
                return null;

            int count = m_state.characters.Count;
            for (int i = 0; i < count; ++i)
            {
                if (m_state.characters[i].ID == characterID)
                    return m_state.characters[i];
            }
            return null;
            //throw new System.Exception("Character not found: " + characterID);
        }


    }
}
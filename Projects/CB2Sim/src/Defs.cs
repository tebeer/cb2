﻿namespace CB2
{
    public class AnimSetDef
    {
        public string attack;
        public string idle;
        public string run;
        public string walk;
        public string death;
        public string takedamage;

        public float runSpeed = 1.0f;
        public float walkSpeed = 1.0f;
        public float hitTime = 0.5f;
    }

    public class CharacterDef
    {
        public float maxHealth;
        public float moveSpd;
        public float radius;
        public string assetName;
        public WeaponDef weapon;
        public int price;
        public int units;
    }

    public enum WeaponType
    {
        Melee,
        Projectile,
    }

    public class WeaponDef
    {
        public float damage;
        public float speed;
        public float range;
        public WeaponType weaponType;
        public AnimSetDef anims;
        public ProjectileDef projectile;
        public float damageInterrupt;
    }

    public class ProjectileDef
    {
        public string assetName;
        public float speed;
    }

    public static class Defs
    {
        public static MapDef TestMap = new MapDef()
        {
            capturePoints = new CapturePointDef[]
            {
                //new CapturePointDef() {
                //    type = CapturePointType.Primary,
                //    captureTime = 30,
                //    position = new Vector3(0, 0, 0),
                //    radius = 8,
                //},
                //new CapturePointDef() {
                //    type = CapturePointType.Secondary,
                //    captureTime = 10,
                //    position = new Vector3(0, 0, 30),
                //    radius = 6,
                //},
                //new CapturePointDef() {
                //    type = CapturePointType.Secondary,
                //    captureTime = 10,
                //    position = new Vector3(0, 0,-30),
                //    radius = 6,
                //},
            },
            spawnPoints = new SpawnPointDef[]
            {
                new SpawnPointDef() {
                    team = 0,
                    position = new Vector3(0, 0, -15),
                },
                new SpawnPointDef() {
                    team = 1,
                    position = new Vector3(0, 0, 15),
                }
            }
        };

        public static AnimSetDef Anim_Hammer = new AnimSetDef()
        {
            idle = "Worker_Idle",
            walk = "Worker_Walk",
            run = "Worker_Run",
            attack = "Worker_Attack",
            death = "Worker_Death",
            runSpeed = 0.4f,
            walkSpeed = 1.0f,
            hitTime = 0.6f,
        };

        public static AnimSetDef Anim_Sword = new AnimSetDef()
        {
            idle = "Sword_Idle_1",
            walk = "Sword_Walk_1",
            run = "Sword_Run_1",
            attack = "Sword_Attack_1",
            death = "Death_1",
            takedamage = "Sword_TakeDamage_1",
            runSpeed = 0.3f,
            walkSpeed = 1.0f,
            hitTime = 0.6f,
        };

        public static AnimSetDef Anim_Cavalry_Sword = new AnimSetDef()
        {
            idle = "Cavalry_Sword_Idle_1",
            walk = "Cavalry_Sword_Walk_1",
            run = "Cavalry_Sword_Run_1",
            attack = "Cavalry_Sword_Attack_1",
            death = "Cavalry_Sword_Death_1",
            takedamage = "Cavalry_Sword_TakeDamage_1",
            runSpeed = 0.3f,
            walkSpeed = 1.0f,
            hitTime = 0.6f,
        };

        public static AnimSetDef Anim_Bow = new AnimSetDef()
        {
            idle = "Bow_Idle_1",
            walk = "Bow_Walk_1",
            run = "Bow_Run_1",
            attack = "Bow_Attack_1",
            death = "Death_1",
            takedamage = "Sword_TakeDamage_1",
            runSpeed = 0.4f,
            walkSpeed = 1.0f,
            hitTime = 0.75f,
        };

        public static ProjectileDef Prj_Arrow = new ProjectileDef()
        {
            speed = 13.0f,
            assetName = "Arrow",
        };

        public static WeaponDef Wpn_Hammer = new WeaponDef()
        {
            damage = 0.5f,
            speed = 0.9f,
            range = 1.5f,
            weaponType = WeaponType.Melee,
            anims = Anim_Hammer,
        };

        public static WeaponDef Wpn_Sword = new WeaponDef()
        {
            damage = 1.26f,
            speed = 1.2f,
            range = 1.5f,
            weaponType = WeaponType.Melee,
            anims = Anim_Sword,
            damageInterrupt = 1.0f,
        };

        public static WeaponDef Wpn_Shortbow = new WeaponDef()
        {
            damage = 1.2f,
            speed = 0.6f,
            range = 14.0f,
            weaponType = WeaponType.Projectile,
            anims = Anim_Bow,
            projectile = Prj_Arrow,
            damageInterrupt = 1.0f,
        };

        public static WeaponDef Wpn_LightCavalry_Sword = new WeaponDef()
        {
            damage = 1.26f,
            speed = 1.0f,
            range = 2.0f,
            weaponType = WeaponType.Melee,
            anims = Anim_Cavalry_Sword,
            damageInterrupt = 1.0f,
        };

        public static CharacterDef Human_Worker = new CharacterDef()
        {
            maxHealth = 3.0f,
            moveSpd = 3.0f,
            weapon = Wpn_Hammer,
            radius = .5f,
            assetName = "Human_Worker",
            price = 200,
        };

        public static CharacterDef Human_LightInfantry = new CharacterDef()
        {
            maxHealth = 20.0f,
            moveSpd = 2.6f,
            weapon = Wpn_Sword,
            radius = .5f,
            assetName = "Human_LightInfantry",
            price = 200,
            units = 4,
        };

        public static CharacterDef Human_Archer = new CharacterDef()
        {
            maxHealth = 12.0f,
            moveSpd = 3.0f,
            weapon = Wpn_Shortbow,
            radius = .5f,
            assetName = "Human_Archer",
            price = 200,
            units = 4,
        };

        public static CharacterDef Human_LightCavalry = new CharacterDef()
        {
            maxHealth = 25.0f,
            moveSpd = 6.0f,
            weapon = Wpn_LightCavalry_Sword,
            radius = 0.75f,
            assetName = "Human_LightCavalry",
            price = 200,
            units = 2,
        };

        public static CharacterDef Undead_Worker = new CharacterDef()
        {
            maxHealth = 3.0f,
            moveSpd = 3.0f,
            weapon = Wpn_Hammer,
            radius = .5f,
            assetName = "Undead_Worker",
            price = 200,
        };

        public static CharacterDef Undead_LightInfantry = new CharacterDef()
        {
            maxHealth = 20.0f,
            moveSpd = 3.0f,
            weapon = Wpn_Sword,
            radius = .5f,
            assetName = "Undead_LightInfantry",
            price = 200,
            units = 4,
        };

        public static CharacterDef Undead_Archer = new CharacterDef()
        {
            maxHealth = 12.0f,
            moveSpd = 3.0f,
            weapon = Wpn_Shortbow,
            radius = .5f,
            assetName = "Undead_Archer",
            price = 200,
            units = 4,
        };

        public static CharacterDef Undead_LightCavalry = new CharacterDef()
        {
            maxHealth = 25.0f,
            moveSpd = 6.0f,
            weapon = Wpn_LightCavalry_Sword,
            radius = .75f,
            assetName = "Undead_LightCavalry",
            price = 200,
            units = 2,
        };

        //public static WeaponDef GolemFist = new WeaponDef()
        //{
        //    damage = 2.3f,
        //    speed = 0.5f,
        //    range = 2.0f,
        //    weaponType = WeaponType.Melee,
        //};
        //
        //public static CharacterDef Golem = new CharacterDef()
        //{
        //    maxHealth = 10.0f,
        //    moveSpd = .8f,
        //    weapon = GolemFist,
        //    radius = 1.0f,
        //    assetName = "Golem",
        //};

        public static int GetCharacterDefID(CharacterDef def)
        {
            for (int i = 0; i < CharacterDefs.Length; ++i)
                if (CharacterDefs[i] == def)
                    return i;
            throw new System.Exception("CharacterDef not found: " + def.assetName);
        }

        public static CharacterDef GetCharacterDef(int defID)
        {
            return CharacterDefs[defID];
        }

        public static CharacterDef[] CharacterDefs =
        {
            Human_Worker,
            Human_LightInfantry,
            Human_Archer,
            Human_LightCavalry,
            Undead_Worker,
            Undead_LightInfantry,
            Undead_Archer,
            Undead_LightCavalry,
        };

        public static CharacterDef[][] PurchasableDefs =
        {
            new CharacterDef[]
            {
                //Human_Worker,
                Human_LightInfantry,
                Human_Archer,
                Human_LightCavalry,
            },
            new CharacterDef[]
            {
                //Undead_Worker,
                Undead_LightInfantry,
                Undead_Archer,
                Undead_LightCavalry,
            },
        };

        public static CharacterDef[] GetPurchasableDefs(int team)
        {
            return PurchasableDefs[team];
        }

        public static int GetProjectileDefID(ProjectileDef def)
        {
            for (int i = 0; i < ProjectileDefs.Length; ++i)
                if (ProjectileDefs[i] == def)
                    return i;
            throw new System.Exception("ProjectileDef not found: " + def.assetName);
        }

        public static ProjectileDef GetProjectileDef(int defID)
        {
            return ProjectileDefs[defID];
        }

        public static ProjectileDef[] ProjectileDefs =
        {
            Prj_Arrow,
        };
    }
}
﻿
namespace CB2
{
    public struct Rotation
    {
        public float x;
        public float i;

        public static Rotation Zero { get { return new Rotation(1, 0); } }

        public Rotation(float x, float i)
        {
            this.x = x;
            this.i = i;
        }

        public static Rotation operator *(Rotation r1, Rotation r2)
        {
            // r1.x * r2.x + r1.x * r2.i + r2.x * r1.i + r1.i * r2.i
            Rotation r;
            r.x = r1.x * r2.x - r1.i * r2.i;
            r.i = r1.x * r2.i + r2.x * r1.i;
            return r;
        }

        public float ToAngle()
        {
            return Math.GetDir(x, i);
        }
    }


    public static class Math
    {
        public const float Rad2Deg = (float)(180.0 / System.Math.PI);
        public const float Deg2Rad = (float)(System.Math.PI / 180);
        public const float PI = (float)System.Math.PI;

        public static float Atan2(float y, float x)
        {
            return (float)System.Math.Atan2(y, x);
        }

        public static float GetDir(float x, float y)
        {
            return -Atan2(y, x) * Math.Rad2Deg;
        }

        public static float Abs(float x)
        {
            return x < 0 ? -x : x;
        }

        public static float Sign(float x)
        {
            return x < 0 ? -1 : 1;
        }

        public static float LerpAngle(float angle, float target, float lerp)
        {
            while (target > angle + 180)
                target -= 360;
            while (target < angle - 180)
                target += 360;

            return angle + (target - angle) * lerp; 
        }

        public static float MoveTowardsAngle(float angle, float target, float amount)
        {
            while (target > angle + 180)
                target -= 360;
            while (target < angle - 180)
                target += 360;

            if (Abs(target - angle) <= amount)
                return target;

            return angle + Sign(target - angle) * amount; 
        }

        public static float Lerp(float v1, float v2, float t)
        {
            return v1 + (v2 - v1) * t;
        }
    }
}

﻿namespace CB2
{
    public class CapturePointDef
    {
        public CapturePointType type;
        public float captureTime;
        public Vector3 position;
        public float radius;
    }

    public class MapDef
    {
        public CapturePointDef[] capturePoints;
        public SpawnPointDef[] spawnPoints;
    }

    public enum CapturePointType
    {
        Primary,
        Secondary,
    }

    public class SpawnPointDef
    {
        public byte team;
        public Vector3 position;
    }
}

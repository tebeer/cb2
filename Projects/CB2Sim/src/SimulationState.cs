﻿using System.Collections.Generic;

namespace CB2
{
    public struct UnitID
    {
        public ushort value;

        public static bool operator==(UnitID u1, UnitID u2)
        {
            return u1.value == u2.value;
        }

        public static bool operator!=(UnitID u1, UnitID u2)
        {
            return u1.value != u2.value;
        }

        public override bool Equals(object obj)
        {
            var u = (UnitID)obj;
            return value.Equals(u.value);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public void Reset()
        {
            value = 0;
        }

        public static UnitID Null
        {
            get
            {
                return new UnitID { value = 0 };
            }
        }

        public bool IsValid
        {
            get
            {
                return value > 0;
            }
        }


        public UnitID Next()
        {
            UnitID id = this;
            value++;
            return id;
        }
    }

    public enum Stance
    {
        HoldPosition,
        Roam,
    }

    public class CharacterSpawn
    {
        public int team;
        public CharacterDef def;
    }

    public class Character
    {
        public UnitID ID;
        public CharacterDef def;
        public State state;

        public struct State
        {
            public Vector3 velocity;
            public Vector3 position;
            public Vector3 balance;
            public float rotation;
            public float attackTimer;
            public Vector3 threat;
        }

        public int team;

        public float targetRotation;
        public UnitID targetCharacterID;

        public Vector3 moveTarget;
        public bool moveToTarget;

        public Stance stance;

        public bool attackDamage;
        public float health;
        public bool destroyed;

        public float takeDamage;
    }

    public class Projectile
    {
        public UnitID ID;
        public float time;
        public float speed;
        public float damage;
        public UnitID targetCharacterID;
        public Vector3 origin;
        public ProjectileDef projectileDef;
    }

    public class CapturePoint
    {
        public CapturePointDef def;
        public float[] capture;
        public bool active;
    }

    public class Map
    {
        public MapDef def;
        public CapturePoint[] capturePoints;
    }

    public struct Team
    {
        public int money;
    }

    public struct SimulationState
    {
        public List<Character> characters;
        public List<Projectile> projectiles;
        public List<CharacterSpawn> spawns;

        public Team[] teams;

        public UnitID nextCharacterID;
        public UnitID nextProjectileID;

        public uint frameNumber;
        public GameState gameState;
        public float stateTimer;

        public RNG random;

        public Map map;
    }
}

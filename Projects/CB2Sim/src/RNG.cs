﻿namespace CB2
{
    public class RNG
    {
        public RNG()
        {
            state.z = 2345264336;
            state.w = 341912093;
        }

        public float GetFloat()
        {
            return (float)GetUniform();
        }

        public uint GetUint()
        {
            state.z = 36969 * (state.z & 65535) + (state.z >> 16);
            state.w = 18000 * (state.w & 65535) + (state.w >> 16);
            return (state.z << 16) + state.w;
        }

        public double GetUniform()
        {
            // 0 <= u < 2^32
            uint u = GetUint();
            // The magic number below is 1/(2^32 + 2).
            // The result is strictly between 0 and 1.
            return (u + 1.0) * 2.328306435454494e-10;
        }

        public State state;

        public struct State
        {
            public uint z;
            public uint w;
        }
    }
}

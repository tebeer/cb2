﻿
Shader "CB/Blur"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Offset("Offset", Vector) = (1, 0, 0, 0)
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Shared.cginc"

            sampler2D _MainTex;
            float4 _MainTex_TexelSize;
            float2 _Offset;

            struct v_in_blur
            {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct f_in_blur
            {
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            f_in_blur vert(v_in_blur v)
            {
                f_in_blur o;
                o.position = UnityObjectToClipPos(v.position);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(f_in_blur i) : SV_Target
            {
                half2 offset = _MainTex_TexelSize * _Offset;

                half4 c1 = tex2D(_MainTex, i.uv - 2*offset);
                half4 c2 = tex2D(_MainTex, i.uv - offset);
                half4 c3 = tex2D(_MainTex, i.uv);
                half4 c4 = tex2D(_MainTex, i.uv + offset);
                half4 c5 = tex2D(_MainTex, i.uv + 2*offset);

                return (c1 + c2 + c3 + c4 + c5) / 5;
            }
            ENDCG
        }
    }
}
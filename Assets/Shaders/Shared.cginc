#include "UnityCG.cginc"

float4x4 CB_ShadowMatrix;
float4x4 CB_AOMatrix;
sampler2D CB_AOMap;
float CB_AOFactor;

float3 CB_SunLightDir;
half3 CB_AmbientLight;
half3 CB_SunLight;

half3 CB_CCF1;
half3 CB_CCF2;
half3 CB_CCF3;


struct v_in
{
    float4 position : POSITION;
    float3 normal : NORMAL;
    fixed3 color : COLOR;
    float2 uv : TEXCOORD0;
};

struct f_in
{
    float4 position : SV_POSITION;
    float3 normal : NORMAL;
    fixed3 color : COLOR;
    float2 uv : TEXCOORD0;
    float3 worldPos : TEXCOORD1;
    float2 uv_ao : TEXCOORD2;
};

float2 WorldToCamera(float4x4 mat, float3 worldPos)
{
    float2 cameraPos = mul(mat, float4(worldPos, 1)).xy;
    cameraPos += 1;
    cameraPos *= .5;

    return cameraPos;
}

//half3 CC(half3 value, half3 f0, half3 f1)
//{
//    value = saturate(value);
//
//    half3 p0 = 0;
//    half3 p1 = 0 + f0;
//    half3 p2 = 1 + f1;
//    half3 p3 = 1;
//
//    half3 p01 = lerp(p0, p1, value);
//    half3 p12 = lerp(p1, p2, value);
//    half3 p23 = lerp(p2, p3, value);
//
//    half3 v0 = lerp(p01, p12, value);
//    half3 v1 = lerp(p12, p23, value);
//
//    return lerp(v0, v1, value);
//}

half3 CC(half3 value)
{
    //f0 *= 3;
    //f1 = 3*(1 + f1);
    //return value*(value*(value*(f0 - f1 + 1) - 2 * f0 + f1) + f0);
    return value*(value*(value*(CB_CCF1) + CB_CCF2) + CB_CCF3);
}

half3 Lighting(half3 color, half3 normal, half shadow, half ao)
{
    half d = dot(normal, -CB_SunLightDir);
    d = saturate(4 * d);
    d *= shadow;

    half3 light = CB_AmbientLight + d * CB_SunLight;
    color *= light;
    
    ao *= (1 - light);//lerp(ao, 0, light);

    color *= (1 - CB_AOFactor * ao);
    //color = lerp(color, .5*color, CB_AOFactor * ao);

    //color.rgb = saturate(color.rgb);
    color = CC(color.rgb);

    return color;
}
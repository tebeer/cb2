﻿
Shader "CB/Shadow_Prepass"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Shared.cginc"

            sampler2D _MainTex;

            struct v_input
            {
                float4 position : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct f_input
            {
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
            };

            f_input vert(v_input v)
            {
                f_input o;
                o.position = UnityObjectToClipPos(v.position);

                o.worldPos = mul(unity_ObjectToWorld, v.position);

                o.uv = v.uv;
                return o;
            }

            fixed4 frag(f_input i) : SV_Target
            {
                half4 c = tex2D(_MainTex, i.uv);

                clip(i.worldPos.y - .1);

                return c.a;
                //return c.a * i.worldPos.y / 5;
            }
            ENDCG
        }
    }
}
﻿
Shader "CB/Opaque"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Color("Color", Color) = (.5, .5, .5, 1)
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Shared.cginc"

            sampler2D _MainTex;
            half4 _MainTex_ST;
            fixed4 _Color;

            f_in vert(v_in v)
            {
                f_in o;
                o.position = UnityObjectToClipPos(v.position);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.color = 2 * _Color * v.color;
                o.worldPos = mul(unity_ObjectToWorld, v.position);
                o.uv_ao = WorldToCamera(CB_AOMatrix, o.worldPos);
                return o;
            }

            fixed4 frag(f_in i) : SV_Target
            {
                half4 col = tex2D(_MainTex, i.uv);
                clip(col.a - .5);

                col.rgb *= i.color;

                col.rgb = lerp(col.rgb, fixed3(.55, .45, .25), .5 * saturate(1 - i.worldPos.y));
                
                half ao = tex2D(CB_AOMap, i.uv_ao).a;
                half ypos = i.worldPos.y;
                if (ypos < 0)
                    ypos = 0;

                ao = saturate(2 * (ao - ypos));
                //ao = saturate(1 - 2*ypos);

                col.rgb = Lighting(col.rgb, i.normal, 1, ao);

                return col;
            }
            ENDCG
        }
    }
}
﻿
Shader "CB/AlphaBlend"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" }
        LOD 100

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        ZTest LEqual

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Shared.cginc"

            sampler2D _MainTex;
            fixed4 _Color;

            struct v_in_shadow
            {
                float4 position : POSITION;
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct f_in_shadow
            {
                float4 position : SV_POSITION;
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            f_in_shadow vert(v_in_shadow v)
            {
                f_in_shadow o;
                o.position = UnityObjectToClipPos(v.position);
                o.uv = v.uv;
                o.color = v.color;
                return o;
            }

            fixed4 frag(f_in_shadow i) : SV_Target
            {
                half4 color = tex2D(_MainTex, i.uv);
                color *= i.color;
                return color;
            }
            ENDCG
        }
    }
}
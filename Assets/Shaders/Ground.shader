﻿
Shader "CB/Ground"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _MainTex2("Texture2", 2D) = "white" {}
        _Splat("Splat", 2D) = "white" {}
        _ShadowMap("Shadowmap", 2D) = "white" {}
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "Shared.cginc"

            sampler2D _MainTex;
            sampler2D _MainTex2;
            sampler2D _Splat;
            sampler2D _ShadowMap;

            float4 _MainTex_ST;
            float4 _MainTex2_ST;
            float4 _ShadowMap_TexelSize;

            struct v_in_ground
            {
                float4 position : POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
            };

            struct f_in_ground
            {
                float4 position : SV_POSITION;
                float3 normal : NORMAL;
                float2 uv_tex1 : TEXCOORD0;
                float2 uv_tex2 : TEXCOORD1;
                float2 uv_splat : TEXCOORD2;
                float2 uv_shadow : TEXCOORD3;
                float2 uv_ao : TEXCOORD4;
            };

            f_in_ground vert(v_in_ground v)
            {
                f_in_ground o;
                o.position = UnityObjectToClipPos(v.position);
                o.normal = UnityObjectToWorldNormal(v.normal);
                o.uv_tex1 = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv_tex2 = TRANSFORM_TEX(v.uv, _MainTex2);
                o.uv_splat = v.uv;

                float3 worldPos = mul(unity_ObjectToWorld, v.position).xyz;

                o.uv_shadow = WorldToCamera(CB_ShadowMatrix, worldPos);
                o.uv_ao = WorldToCamera(CB_AOMatrix, worldPos);
                return o;
            }

            fixed4 frag(f_in_ground i) : SV_Target
            {
                half4 col1 = tex2D(_MainTex, i.uv_tex1);
                half4 col2 = tex2D(_MainTex2, i.uv_tex2);
                half4 splat = tex2D(_Splat, i.uv_splat);

                half h1 = splat.r + 4*col1.a;//4*col1.a * splat.r;
                half h2 = splat.g + 4*col2.a;//splat.g;//(.3 + col2.a) * splat.g;

                half4 col = lerp(col1, col2, saturate(0.5 + 2*(h2 - h1)));

                //half shadow = -.5
                //    + .5 * tex2Dlod(_ShadowMap, float4(i.uv_shadow, 0, 0)).a
                //    + tex2Dlod(_ShadowMap, float4(i.uv_shadow, 2, 2)).a
                //    ;
                //half2 so = _ShadowMap_TexelSize.xy;
                //half shadow = 0
                //    + .2 * tex2D(_ShadowMap, i.uv_shadow).a
                //    + .2 * tex2D(_ShadowMap, i.uv_shadow + half2(-so.x, 0)).a
                //    + .2 * tex2D(_ShadowMap, i.uv_shadow + half2( so.x, 0)).a
                //    + .2 * tex2D(_ShadowMap, i.uv_shadow + half2( 0,-so.y)).a
                //    + .2 * tex2D(_ShadowMap, i.uv_shadow + half2( 0, so.y)).a;
                //shadow = (shadow - .2) / .8;
                half shadow = tex2D(_ShadowMap, i.uv_shadow).a;

                half ao = saturate(2*tex2D(CB_AOMap, i.uv_ao).a);

                col.rgb = Lighting(col.rgb, i.normal, saturate(1.0 - shadow), ao);

                return col;
            }
            ENDCG
        }
    }
}
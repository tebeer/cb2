﻿using System.Collections.Generic;
using UnityEngine;

public struct Events
{
    public event System.Action EvtSimulationChanged;
    public event System.Action<int> EvtSelectGroup;
    public event System.Action<int> EvtAddToGroup;
    public event System.Action<CB2.CharacterDef> EvtPurchase;
    public event System.Action<CB2.Stance> EvtSetStance;
    public event System.Action EvtConnect;
    public event System.Action EvtLocalServer;
    public event System.Action EvtDisconnect;
    public event System.Action EvtRestart;

    public void SelectGroup(int i)
    {
        if (EvtSelectGroup != null)
            EvtSelectGroup(i);
    }

    public void AddToGroup(int i)
    {
        if (EvtAddToGroup != null)
            EvtAddToGroup(i);
    }

    public void SimulationChanged()
    {
        if (EvtSimulationChanged != null)
            EvtSimulationChanged();
    }

    public void Purchase(CB2.CharacterDef def)
    {
        if (EvtPurchase != null)
            EvtPurchase(def);
    }

    public void SetStance(CB2.Stance stance)
    {
        if (EvtSetStance != null)
            EvtSetStance(stance);
    }

    public void Connect()
    {
        if (EvtConnect != null)
            EvtConnect();
    }

    public void LocalServer()
    {
        if (EvtLocalServer != null)
            EvtLocalServer();
    }

    public void Disconnect()
    {
        if (EvtDisconnect != null)
            EvtDisconnect();
    }

    public void Restart()
    {
        if (EvtRestart != null)
            EvtRestart();
    }
}

public class ConnectionStatus
{
    public bool isConnected;
    public string address;
    public bool isServer;
    public bool desync;
}

public class GameData
{
    public Events events;
    public CB2.Simulation simulation;
    public ConnectionStatus connectionStatus = new ConnectionStatus();

    public List<CharacterGraphics> graphics = new List<CharacterGraphics>();
    public List<CharacterGraphics> deaths = new List<CharacterGraphics>();
    //public List<CB2.Character> selected = new List<CB2.Character>();
    public List<CB2.Character>[] controlGroups = new List<CB2.Character>[5];

    public uint lastFrame;
    public float lerpTime;
    public float lerp;
    public bool wasUpdated;
    public int ourTeam;
    public int activeGroup;

    public GameData()
    {
        for (int i = 0; i < controlGroups.Length; ++i)
            controlGroups[i] = new List<CB2.Character>();
    }
}

public class CharacterGraphics
{
    public CB2.Character character;
    public CB2.Character.State currentState;
    public CB2.Character.State lastState;
    public CB2.Character.State lerpState;
    public GameObject go;
    public Animation anim;
    public float deathTimer;
}
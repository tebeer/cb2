﻿using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    public static int BottomBarSize;
    public Button[] UnitButtons;
    public Button ChargeButton;
    public Button HoldPosButton;
    public Text MoneyText;
    public Text TimerText;

    private bool IsRunningState { get { return m_data.connectionStatus.isConnected && m_data.simulation != null && m_data.simulation.State.gameState == CB2.GameState.Running; } }
    private bool IsBuildingState { get { return m_data.connectionStatus.isConnected && m_data.simulation != null && m_data.simulation.State.gameState == CB2.GameState.Start; } }

    public void SetData(GameData data)
    {
        m_data = data;

        var defs = CB2.Defs.GetPurchasableDefs(m_data.ourTeam);
        for(int i = 0; i < defs.Length; ++i)
        {
            var index = i;
            var def = defs[index];
            UnitButtons[i].GetComponentInChildren<Text>().text = string.Format("{0} {1}", def.price, def.assetName);
            UnitButtons[i].onClick.AddListener(() =>
            {
                if(m_data.connectionStatus.isConnected && m_data.simulation != null)
                {
                    if(IsBuildingState)
                    {
                        m_data.activeGroup = index;
                        m_data.events.Purchase(def);
                    }
                    else if(IsRunningState)
                    {
                        m_data.events.SelectGroup(index);
                    }
                }
            });
        }

        ChargeButton.onClick.AddListener(() =>
        {
            if (IsRunningState)
                m_data.events.SetStance(CB2.Stance.Roam);
        });
        HoldPosButton.onClick.AddListener(() =>
        {
            if (IsRunningState)
                m_data.events.SetStance(CB2.Stance.HoldPosition);
        });
    }

    void Update()
    {
        if(m_data.connectionStatus.isConnected && m_data.simulation != null)
        {
            MoneyText.text = "Money: " + m_data.simulation.State.teams[m_data.ourTeam].money;
            int seconds = (int)(m_data.simulation.State.stateTimer + 1);
            TimerText.text = seconds.ToString();
        }
    }

    void OnGUI()
    {
        //if (m_data.connectionStatus.isConnected && m_data.simulation != null)
        //{
        //    GameUI();
        //}

        ConnectionDialog(m_data.connectionStatus);
    }

    void GameUI()
    {
        //int seconds = (int)(m_data.simulation.State.stateTimer + 1);
        //GUI.Label(new Rect(Screen.width / 2 - Screen.height * .1f * .5f, 0, Screen.height * .1f, Screen.height * .05f), seconds.ToString(), Styles.Box);
        BottomBar();
    }

    void BottomBar()
    {
        BottomBarSize = (int)(Screen.height * .125f);

        GUILayout.BeginArea(new Rect(0, Screen.height - BottomBarSize, Screen.width, BottomBarSize), Styles.Box);
        
        GUILayout.Label("Money: " + m_data.simulation.State.teams[m_data.ourTeam].money, Styles.Label);

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();

        var defs = CB2.Defs.GetPurchasableDefs(m_data.ourTeam);
        for (int i = 0; i < defs.Length; ++i)
        {
            if (GUILayout.Button(string.Format("{0} {1}", defs[i].price, defs[i].assetName), Styles.ButtonLeft))
            {
                if(m_data.simulation.State.gameState == CB2.GameState.Start)
                {
                    m_data.activeGroup = i;
                    m_data.events.Purchase(defs[i]);
                }
                else if(m_data.simulation.State.gameState == CB2.GameState.Running)
                {
                    m_data.events.SelectGroup(i);
                }
            }
        }

        GUILayout.EndHorizontal();

        //GUILayout.BeginHorizontal();
        //for (int i = 0; i < m_data.controlGroups.Length; ++i)
        //{
        //    GUILayout.BeginVertical();
        //
        //    if (GUILayout.Button(string.Format("{0}", m_data.controlGroups[i].Count), Styles.LargeButton, GUILayout.ExpandHeight(true)))
        //    {
        //        m_data.events.SelectGroup(i);
        //    }
        //
        //    //if (GUILayout.Button("Add", Styles.LargeButton, GUILayout.ExpandHeight(true)))
        //    //{
        //    //    m_data.events.AddToGroup(i);
        //    //}
        //
        //    GUILayout.EndVertical();
        //}
        //GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    void ConnectionDialog(ConnectionStatus status)
    {
        if (!status.isConnected)
        {
            GUILayout.BeginVertical(GUI.skin.box);

            GUILayout.BeginHorizontal();
            GUILayout.Label("IP");
            status.address = GUILayout.TextField(status.address, Styles.InputField, GUILayout.Width(100));

            if (GUILayout.Button("Connect", Styles.LargeButton))
            {
                m_data.events.Connect();

            }
            GUILayout.EndHorizontal();

            if (GUILayout.Button("Local server", Styles.LargeButton))
            {
                m_data.events.LocalServer();
            }

            GUILayout.EndVertical();
        }
        else
        {
            GUILayout.BeginVertical(GUI.skin.box);

            if (GUILayout.Button("Quit", Styles.LargeButton))
            {
                m_data.events.Disconnect();
            }

            if (m_data.connectionStatus.isServer)
            {
                if (GUILayout.Button("Restart", Styles.LargeButton))
                {
                    m_data.events.Restart();
                }
            }

            if (m_data.connectionStatus.desync)
            {
                GUILayout.Label("Desync");
            }
            GUILayout.EndVertical();
        }
    }

    private GameData m_data;

}
public static class Styles
{
    public static GUIStyle InputField
    {
        get
        {
            if (m_inputField == null)
                m_inputField = new GUIStyle(GUI.skin.textField);

            m_inputField.fontSize = (int)(Screen.height * .03f);

            return m_inputField;
        }
    }

    public static GUIStyle LargeButton
    {
        get
        {
            if (m_largeButton == null)
                m_largeButton = new GUIStyle(GUI.skin.button);

            m_largeButton.fontSize = (int)(Screen.height * .03f);

            return m_largeButton;
        }
    }

    public static GUIStyle ButtonLeft
    {
        get
        {
            if (m_buttonLeft == null)
                m_buttonLeft = new GUIStyle(GUI.skin.button);

            m_buttonLeft.fontSize = (int)(Screen.height * .02f);
            m_buttonLeft.fixedHeight = (int)(Screen.height * .04f);
            m_buttonLeft.alignment = TextAnchor.MiddleLeft;

            return m_buttonLeft;
        }
    }

    public static GUIStyle Label
    {
        get
        {
            if (m_label == null)
                m_label = new GUIStyle(GUI.skin.label);

            m_label.fontSize = (int)(Screen.height * .02f);
            m_label.fixedHeight = (int)(Screen.height * .025f);
            m_label.alignment = TextAnchor.MiddleLeft;

            return m_label;
        }
    }

    public static GUIStyle Box
    {
        get
        {
            if (m_box == null)
                m_box = new GUIStyle(GUI.skin.box);

            m_box.alignment = TextAnchor.MiddleCenter;
            m_box.normal.textColor = Color.white;
            m_box.fontSize = (int)(Screen.height * .03f);

            return m_box;
        }
    }
    private static GUIStyle m_inputField;
    private static GUIStyle m_largeButton;
    private static GUIStyle m_buttonLeft;
    private static GUIStyle m_label;
    private static GUIStyle m_box;

}

﻿using UnityEngine;

[ExecuteInEditMode]
public class ShaderController : MonoBehaviour
{
    public Camera ShadowCamera;
    public float ShadowBlur = 1;
    public int ShadowBlurIterations = 1;

    public Camera AOCamera;
    public float AOBlur = 1;
    public int AOBlurIterations = 1;
    public float AOFactor = 1;
    public Color SunLight = new Color(1f, .9f, .8f);
    public Color AmbientLight = new Color(.7f, .8f, 1f);
    public Vector3 CCMin = new Vector3(.4f, .55f, .7f);
    public Vector3 CCMax = new Vector3(-.0f, -.2f, -.3f);

    public Shader AOPrepassShader;
    public Shader ShadowPrepassShader;
    public Material Blur;

    [System.NonSerialized]
    private bool m_initialized;

    private bool m_update;

    void Initialize()
    {
        m_initialized = true;
        m_update = true;
        Camera.onPreRender += PreRender;
        Camera.onPostRender += PostRender;
    }

    void OnDestroy()
    {
        Camera.onPreRender -= PreRender;
        Camera.onPostRender -= PostRender;
    }

    private void PostRender(Camera cam)
    {
    }

    private void PreRender(Camera cam)
    {
        if (cam == Camera.main)
        {
            Shader.SetGlobalVector("CB_SunLightDir", ShadowCamera.transform.forward);
            Shader.SetGlobalColor("CB_AmbientLight", AmbientLight);
            Shader.SetGlobalColor("CB_SunLight", SunLight);

            var f0 = 3 * CCMin;
            var f1 = 3 * (Vector3.one + CCMax);
            Shader.SetGlobalVector("CB_CCF1", f0 - f1 + Vector3.one);
            Shader.SetGlobalVector("CB_CCF2", -2 * f0 + f1);
            Shader.SetGlobalVector("CB_CCF3", f0);

            if (m_update)
            {
                RenderAO();
                RenderShadows();
                m_update = false;
            }
        }
    }

    private void RenderAO()
    {
        var aoMatrix = AOCamera.projectionMatrix * AOCamera.worldToCameraMatrix;
        Shader.SetGlobalMatrix("CB_AOMatrix", aoMatrix);
        Shader.SetGlobalTexture("CB_AOMap", AOCamera.targetTexture);
        Shader.SetGlobalFloat("CB_AOFactor", AOFactor);

        AOCamera.SetReplacementShader(AOPrepassShader, "");
        AOCamera.Render();
        var aoTex = AOCamera.targetTexture;
        var temp = RenderTexture.GetTemporary(aoTex.width / 2, aoTex.height / 2, 0);
        for (int i = 0; i < AOBlurIterations; ++i)
        {
            Blur.SetVector("_Offset", new Vector2(2 * AOBlur, 0));
            Graphics.Blit(aoTex, temp, Blur);
            Blur.SetVector("_Offset", new Vector2(0, AOBlur));
            Graphics.Blit(temp, aoTex, Blur);
        }
        RenderTexture.ReleaseTemporary(temp);
    }

    private void RenderShadows()
    {
        var shadowMatrix = ShadowCamera.projectionMatrix * ShadowCamera.worldToCameraMatrix;
        Shader.SetGlobalMatrix("CB_ShadowMatrix", shadowMatrix);

        ShadowCamera.SetReplacementShader(ShadowPrepassShader, "");
        ShadowCamera.Render();
        var shadowTex = ShadowCamera.targetTexture;
        var temp = RenderTexture.GetTemporary(shadowTex.width, shadowTex.height, 0);
        for (int i = 0; i < ShadowBlurIterations; ++i)
        {
            Blur.SetVector("_Offset", new Vector2(ShadowBlur, 0));
            Graphics.Blit(shadowTex, temp, Blur);
            Blur.SetVector("_Offset", new Vector2(0, ShadowBlur));
            Graphics.Blit(temp, shadowTex, Blur);
        }
        RenderTexture.ReleaseTemporary(temp);
    }

    void Update()
    {
        if (!m_initialized)
        {
            Initialize();
        }

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class CommandController : MonoBehaviour
{

    public void SetData(GameData data)
    {
        m_data = data;
        m_data.events.EvtSelectGroup += SelectGroup;
        m_data.events.EvtAddToGroup += AddToGroup;
        m_data.events.EvtPurchase += Purchase;
        m_data.events.EvtSetStance += SetStance;
    }

    public List<CB2.Command> GetCommands()
    {
        return m_commands;
    }

    public void ClearCommands()
    {
        m_commands.Clear();
    }

    private List<CB2.Command> m_commands = new List<CB2.Command>();

    private Vector2 m_selectionStart;
    private List<Vector3> m_movePoints = new List<Vector3>();

    //private void SelectSameType()
    //{
    //    var types = new List<CB2.CharacterDef>();
    //    foreach (var character in m_data.selected)
    //    {
    //        if (!types.Contains(character.def))
    //            types.Add(character.def);
    //    }
    //
    //    foreach (var character in m_data.simulation.Characters)
    //    {
    //        if (character.team == m_data.ourTeam && !m_data.selected.Contains(character) && types.Contains(character.def))
    //        {
    //            m_data.selected.Add(character);
    //        }
    //    }
    //}

    private void UpdateGroups()
    {
        for (int i = 0; i < m_data.controlGroups.Length; ++i)
        {
            var group = m_data.controlGroups[i];
            group.RemoveAll(c => c.destroyed || !m_data.simulation.Characters.Contains(c));
        }

        var unitDefs = CB2.Defs.GetPurchasableDefs(m_data.ourTeam);

        foreach (var character in m_data.simulation.Characters)
        {
            if (character.team == m_data.ourTeam)
            {
                bool isInGroup = false;
                for (int i = 0; i < m_data.controlGroups.Length; ++i)
                {
                    if (m_data.controlGroups[i].Contains(character))
                    {
                        isInGroup = true;
                        break;
                    }
                }
                if(!isInGroup)
                {
                    int groupIndex;
                    for (groupIndex = 0; groupIndex < unitDefs.Length; ++groupIndex)
                    {
                        if (character.def == unitDefs[groupIndex])
                            break;
                    }

                    if(groupIndex < unitDefs.Length)
                        m_data.controlGroups[groupIndex].Add(character);
                }
            }
        }

        //if (m_data.selected.Count == 0)
        //    //m_data.controlGroups[m_data.activeGroup].Count == 0)
        //    m_data.activeGroup = 0;
    }

    private void AddToGroup(int groupIndex)
    {
    //    m_data.activeGroup = groupIndex;
    //
    //    if (groupIndex == 0)
    //        return;
    //
    //    for (int i = 1; i < m_data.controlGroups.Length; ++i)
    //    {
    //        var group = m_data.controlGroups[i];
    //        foreach (var c in m_data.selected)
    //            group.Remove(c);
    //    }
    //
    //   // m_data.controlGroups[groupIndex].Clear();
    //    m_data.controlGroups[groupIndex].AddRange(m_data.selected);
    }

    private void SelectGroup(int groupIndex)
    {
        m_data.activeGroup = groupIndex;
        //m_data.selected.Clear();
        //m_data.selected.AddRange(m_data.controlGroups[groupIndex]);
    }

    private void Purchase(CB2.CharacterDef character)
    {
        m_commands.Add(new CB2.Command()
        {
            type = CB2.CommandType.Purchase,
            characterID = new CB2.UnitID { value = (ushort)CB2.Defs.GetCharacterDefID(character) },
        });
    }

    private void SetStance(CB2.Stance stance)
    {
        var units = m_data.controlGroups[m_data.activeGroup];
        for (int i = 0; i < units.Count; ++i)
        {
            var character = units[i];
            m_commands.Add(new CB2.Command()
            {
                type = CB2.CommandType.SetStance,
                stance = stance,
                characterID = character.ID,
            });
        }
    }

    private enum CommandType
    {
        Select,
        Move
    }

    private CommandType m_currentCommand;

    //void StartSelect()
    //{
    //    m_selectionStart = Input.mousePosition;
    //}
    //
    //void UpdateSelect()
    //{
    //}

    //void EndSelect()
    //{
    //    Vector2 min = m_selectionStart;
    //    Vector2 max = Input.mousePosition;
    //    if (min.x > max.x)
    //    {
    //        float temp = min.x;
    //        min.x = max.x;
    //        max.x = temp;
    //    }
    //    if (min.y > max.y)
    //    {
    //        float temp = min.y;
    //        min.y = max.y;
    //        max.y = temp;
    //    }
    //
    //    m_data.selected.Clear();
    //    foreach (var character in m_data.simulation.Characters)
    //    {
    //        if (character.team == m_data.ourTeam)
    //        {
    //            Vector2 screenPos = Camera.main.WorldToScreenPoint(character.state.position.ToUnity());
    //            if (screenPos.x > min.x && screenPos.x < max.x && screenPos.y > min.y && screenPos.y < max.y)
    //            {
    //                m_data.selected.Add(character);
    //            }
    //        }
    //    }
    //
    //    m_currentCommand = CommandType.Move;
    //}

    void StartMove()
    {
        m_movePoints.Clear();
    }

    void UpdateMove()
    {
        Plane plane = new Plane(Vector3.up, 0);
        var screenRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hit;
        if (plane.Raycast(screenRay, out hit))
        {
            Vector3 hitPos = (screenRay.origin + hit * screenRay.direction);
            if (m_movePoints.Count == 0)
            {
                m_movePoints.Add(hitPos);
            }
            else
            {
                var lastPos = m_movePoints[m_movePoints.Count - 1];
                Vector3 dif = (hitPos - lastPos);
                float distance = dif.magnitude;
                while(distance > 1)
                {
                    Vector3 dir = dif/distance;

                    lastPos += dir;
                    distance -= 1;
                    m_movePoints.Add(lastPos);
                }

            }
        }
    }

    void EndMove()
    {
        //if (m_movePoints.Count < 2)
        //{
        //    m_currentCommand = CommandType.Select;
        //    m_data.selected.Clear();
        //}
        var units = m_data.controlGroups[m_data.activeGroup];

        if (units.Count == 1)
        {
            CB2.Command cmd = new CB2.Command();
            cmd.characterID = units[0].ID;
            cmd.target = m_movePoints[0].ToCB();
            cmd.type = CB2.CommandType.Move;
            m_commands.Add(cmd);
        }
        else
        {
            var points = new List<CB2.Vector3>();

            float totalDist = 0;
            for (int i = 1; i < m_movePoints.Count; ++i)
            {
                var p0 = m_movePoints[i - 1];
                var p1 = m_movePoints[i];
                totalDist += (p1 - p0).magnitude;
            }

            int rowCount = (int)(((units.Count) * units[0].def.radius * 2) / totalDist);

            if (rowCount <= 0)
                rowCount = 1;

            for (int i = 0; i < units.Count; i += rowCount)
            {
                //if (m_movePoints.Count > 1)
                //{
                    float f = (float)i / (units.Count - 1);
                    f = f * (m_movePoints.Count - 1);
                    int pi = (int)f;
                    float lerp = f - pi;
                    var point = Vector3.Lerp(m_movePoints[Mathf.Max(pi, 0)], m_movePoints[Mathf.Min(pi + 1, m_movePoints.Count - 1)], lerp);
                    for (int r = 0; r < rowCount; ++r)
                    {
                        points.Add((point + new Vector3(0, 0, r * units[0].def.radius * 2)).ToCB() );
                    }
                //}
                //else
                //{
                //    points.Add(m_movePoints[0].ToCB());
                //}
            }
            for (int i = 0; i < units.Count; ++i)
            {
                var character = units[i];
                //character.moveToTarget = true;
                //character.stance = CB2.Simulation.Stance.HoldPosition;

                int closestPoint = -1;
                float closestDist = float.MaxValue;
                for (int j = 0; j < points.Count; ++j)
                {
                    var dist = (character.state.position - points[j]).magnitude;
                    if (dist < closestDist)
                    {
                        closestPoint = j;
                        closestDist = dist;
                    }
                }
                if (closestPoint >= 0)
                {
                    //character.moveTarget = points[closestPoint];

                    CB2.Command cmd = new CB2.Command();
                    cmd.characterID = character.ID;
                    cmd.target = points[closestPoint];
                    cmd.type = CB2.CommandType.Move;
                    m_commands.Add(cmd);

                    points.RemoveAt(closestPoint);
                }
            }
        }
    }

    void Update()
    {
        if (m_data.simulation == null)
            return;

        //m_data.selected.RemoveAll(c => c.destroyed);
        UpdateGroups();

        if (Input.mousePosition.y < Screen.height*.2f)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
            Purchase(CB2.Defs.Human_LightInfantry);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            Purchase(CB2.Defs.Human_Archer);
        if (Input.GetKeyDown(KeyCode.Alpha3))
            Purchase(CB2.Defs.Human_LightCavalry);

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    SelectSameType();
        //}

        if (Input.GetMouseButtonDown(0))
        {
            //if (m_data.selected.Count == 0)
            //    m_currentCommand = CommandType.Select;
            //
            //if (m_currentCommand == CommandType.Select)
            //    StartSelect();
            //else if (m_currentCommand == CommandType.Move)
                StartMove();
        }

        if (Input.GetMouseButton(0))
        {
            //if (m_currentCommand == CommandType.Select)
            //    UpdateSelect();
            //else if (m_currentCommand == CommandType.Move)
                UpdateMove();
        }

        if(Input.GetMouseButtonUp(0))
        {
            //if (m_currentCommand == CommandType.Select)
            //    EndSelect();
            //else if (m_currentCommand == CommandType.Move)
                EndMove();
        }
    }

    private GameData m_data;
}

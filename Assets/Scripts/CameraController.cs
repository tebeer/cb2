﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform CameraCenter;
    public Transform MainCamera;
    public Transform ShadowCenter;
    public Camera ShadowCamera;
    public Transform AOCenter;
    public Camera AOCamera;

    public void SetData(GameData data)
    {
        m_data = data;
    }

    private void Expand(ref Vector3 min, ref Vector3 max, Vector3 pos)
    {
        if (pos.x < min.x)
            min.x = pos.x;
        if (pos.y < min.y)
            min.y = pos.y;
        if (pos.z < min.z)
            min.z = pos.z;

        if (pos.x > max.x)
            max.x = pos.x;
        if (pos.y > max.y)
            max.y = pos.y;
        if (pos.z > max.z)
            max.z = pos.z;
    }
	
	void Update ()
    {
        if (m_data == null || m_data.simulation == null)
            return;

        var centerPos = CameraCenter.transform.localPosition;
        var cameraPos = MainCamera.transform.localPosition;
        var shadowCameraCenter = ShadowCenter.transform.localPosition;
        var shadowCameraPos = ShadowCamera.transform.localPosition;
        var aoCameraCenter = AOCenter.transform.localPosition;
        var aoCameraPos = AOCamera.transform.localPosition;

        Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
        Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);

        //var characterList = m_data.selected;
        //if (characterList.Count == 0)
        //    characterList = m_data.controlGroups[0];

        //foreach (var character in characterList)
        //{
        //    Vector3 pos = character.state.position.ToUnity();
        //    Expand(ref min, ref max, pos);
        //
        //    if(//characterList == m_data.selected && 
        //        character.moveToTarget)
        //        Expand(ref min, ref max, character.moveTarget.ToUnity());
        //}

        //if (characterList.Count == 0)
        {
            //foreach (var point in m_data.simulation.State.map.capturePoints)
            //    Expand(ref min, ref max, point.def.position.ToUnity());
            foreach (var point in m_data.simulation.State.map.def.spawnPoints)
                Expand(ref min, ref max, point.position.ToUnity());
            //min = max = Vector3.zero;
        }

        float targetBorder = 10;
        min.x -= targetBorder;
        min.z -= targetBorder;
        max.x += targetBorder;
        max.z += targetBorder;

        var size = Mathf.Max(20, Mathf.Abs(max.x - min.x), Mathf.Abs(max.z - min.z) * Mathf.Sin(CameraCenter.eulerAngles.x * Mathf.Deg2Rad));
        float screenHeight = Screen.height - GUIController.BottomBarSize;

        var aspect = Camera.main.aspect;
        if (aspect > 1.0f)
            aspect = 1.0f;

        var tan = Mathf.Tan(Camera.main.fieldOfView * aspect * Mathf.Deg2Rad);
        float targetDistance = size*.5f / tan;

        Vector3 targetPos = .5f * (max + min);
        targetPos.z -= (float)GUIController.BottomBarSize / Screen.height * (max.z - min.z);

        cameraPos.z += 10 * Time.deltaTime * (-targetDistance - cameraPos.z);
        centerPos += 10 * Time.deltaTime * (targetPos - centerPos);

        centerPos.y = 0;

        ShadowCamera.orthographicSize = cameraPos.z * .5f;
        AOCamera.orthographicSize = cameraPos.z * .5f;

        shadowCameraCenter = centerPos;
        aoCameraCenter = centerPos;

        CameraCenter.transform.localPosition = centerPos;
        MainCamera.transform.localPosition = cameraPos;
        ShadowCenter.transform.localPosition = shadowCameraCenter;
        ShadowCamera.transform.localPosition = shadowCameraPos;
        AOCenter.transform.localPosition = aoCameraCenter;
        AOCamera.transform.localPosition = aoCameraPos;
	}


    private GameData m_data;
}

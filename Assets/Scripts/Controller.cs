﻿using UnityEngine;
using CB2;
using CB2.Networking;

public class Controller : MonoBehaviour
{
    public GraphicsController graphicsController;
    public CommandController commandsController;
    public CameraController cameraController;
    public GUIController guiController;

    private GameData m_data = new GameData();

    void Start()
    {
        Screen.fullScreen = false;

        m_data = new GameData();

        m_data.events.EvtConnect += Connect;
        m_data.events.EvtLocalServer += LocalServer;
        m_data.events.EvtDisconnect += Disconnect;
        m_data.events.EvtRestart += Restart;

        m_data.connectionStatus.address = "127.0.0.1";

        graphicsController.SetData(m_data);
        commandsController.SetData(m_data);
        cameraController.SetData(m_data);
        guiController.SetData(m_data);
    }

    void OnDestroy()
    {
        Disconnect();
    }

    void Update()
    {
        if (m_server != null)
        {
            m_server.Update();
        }

        if (m_client != null)
        {
            m_data.connectionStatus.isConnected = m_client.IsConnected;
            m_data.connectionStatus.desync = m_client.Desync;

            m_data.ourTeam = m_client.OurTeam;

            m_client.SendCommands(commandsController.GetCommands());
            commandsController.ClearCommands();

            m_client.Update();

            if (m_client.Sim != m_data.simulation)
            {
                m_data.simulation = m_client.Sim;
                m_data.events.SimulationChanged();
            }
        }
        else
        {
            if (m_data.simulation != null)
            {
                m_data.simulation = null;
                m_data.events.SimulationChanged();
            }
        }

        //if (Input.GetKeyDown(KeyCode.F) && m_characters.Count > 0)
        //{
        //    int index = 0;
        //    for (int i = 0; i < m_characters.Count; ++i)
        //    {
        //        if (followedCharacter == m_characters[i])
        //        {
        //            index = i;
        //            break;
        //        }
        //    }
        //    index = (index + 1) % m_characters.Count;
        //
        //    followedCharacter = m_characters[index];
        //}
        //
        //if (followedCharacter != null)
        //{
        //    followCamera.transform.position = followedCharacter.position + followCamera.transform.rotation * new Vector3(0, 0, -5);
        //
        //}
    }

    private void Connect()
    {
        if (m_client == null)
            m_client = new CB2Client();

        m_client.Log = ClientLog;
        m_client.Connect(m_data.connectionStatus.address);
    }

    private void LocalServer()
    {
        m_server = new CB2Server(ServerLog);
        m_data.connectionStatus.isServer = true;
        m_data.connectionStatus.address = "127.0.0.1";

        Connect();
    }

    private void ServerLog(string log)
    {
        Debug.Log("CB2Server: " + log);
    }

    private void ClientLog(string log)
    {
        Debug.Log("CB2Client: " + log);
    }

    private void Disconnect()
    {
        if(m_client != null)
            m_client.Destroy();
        if (m_server != null)
            m_server.Destroy();
        m_client = null;
        m_server = null;

        m_data.connectionStatus.isConnected = false;
        m_data.connectionStatus.isServer = false;
        m_data.connectionStatus.desync = false;
    }

    void Restart()
    {
        m_server.RestartGame();
    }

    private CB2Client m_client;
    private CB2Server m_server;
}
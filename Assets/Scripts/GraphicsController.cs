﻿using System.Text.RegularExpressions;
using UnityEngine;

public class GraphicsController : MonoBehaviour
{
    public GameObject Shadow;
    public GameObject selectionPrefab;
    public Shader ShadowShader;
    public Shader AlphaBlendShader;
    public Shader AdditiveShader;
    public Sprite SelectionSprite;
    public Sprite BlobShadowSprite;
    public Sprite CapturePointSprite;
    public Sprite CircleSprite;

    public void SetData(GameData data)
    {
        m_data = data;
        m_data.events.EvtSimulationChanged += OnSimulationChanged;
    }

    private void OnSimulationChanged()
    {
        if(m_data.simulation != null)
            m_data.lastFrame = m_data.simulation.State.frameNumber;

        m_data.lerp = 0;
        m_data.lerpTime = 0;
        m_data.wasUpdated = false;

        foreach (var g in m_data.graphics)
            Destroy(g.go);

        foreach (var g in m_data.deaths)
            Destroy(g.go);

        m_data.graphics.Clear();
        m_data.deaths.Clear();
    }

    private GameObject GetPrefab(string name)
    {
        return System.Array.Find(m_prefabs, p => p.name == name);
    }

    void Awake()
    {
        m_selectionMaterial = selectionPrefab.GetComponent<MeshRenderer>().sharedMaterial;
        m_seletionMesh = selectionPrefab.GetComponent<MeshFilter>().sharedMesh;
        m_selectionScale = selectionPrefab.transform.lossyScale;
        m_prefabs = Resources.LoadAll<GameObject>("Prefabs");

        Camera.onPostRender += PostRender;

        m_spriteBatch = new SpriteBatch(false);
       // m_spriteBatch.Matrix = Matrix4x4.Rotate(Quaternion.Euler(90, 0, 0));
    }

    void OnDestroy()
    {
        Camera.onPostRender -= PostRender;

        m_spriteBatch.Destroy();
    }

    void Update()
    {
        m_spriteBatch.Clear();

        if (m_data.simulation == null)
            return;

        m_data.lerpTime += Time.deltaTime;
        m_data.wasUpdated = false;

        if (m_data.simulation.State.frameNumber != m_data.lastFrame)
        {
            m_data.lastFrame = m_data.simulation.State.frameNumber;
            m_data.lerpTime -= CB2.Simulation.DeltaTime;
            m_data.wasUpdated = true;
            if (m_data.lerpTime < 0)
                m_data.lerpTime = 0;
            //if (m_lerp > CB2.Simulation.DeltaTime)
            //    m_lerp = 0;
        }

        m_data.lerp = m_data.lerpTime / CB2.Simulation.DeltaTime;
        //Debug.Log(m_lerpTime + " " +CB2.Simulation.DeltaTime + " " + m_lerp);

        for (int i = m_data.graphics.Count - 1; i >= 0; --i)
        {
            if (m_data.graphics[i].character.destroyed)
            {
                m_data.deaths.Add(m_data.graphics[i]);
                m_data.graphics.RemoveAt(i);
            }
        }

        var characters = m_data.simulation.Characters;
        var count = characters.Count;
        for(int i = 0;  i < count; ++i)
        {
            var character = characters[i];

            if (i >= m_data.graphics.Count)
            {
                CharacterGraphics graphics = new CharacterGraphics();

                graphics.character = character;

                GameObject prefab = GetPrefab(character.def.assetName);

                graphics.go = GameObject.Instantiate(prefab);
                graphics.anim = graphics.go.GetComponent<Animation>();
                graphics.currentState = character.state;
                graphics.lastState = character.state;
                //var shadow = Instantiate(Shadow, graphics.go.transform, false);

                m_data.graphics.Add(graphics);
            }

            if (m_data.graphics[i].character != character)
            {
                //throw new System.Exception("DESYNC");
                m_data.graphics[i].character = character;
            }

            UpdateGraphics(m_data.graphics[i]);
        }

        for (int i = 0; i < m_data.simulation.Projectiles.Count; ++i)
        {
            var projectile = m_data.simulation.Projectiles[i];
            var prefab = GetPrefab(projectile.projectileDef.assetName);
            var renderer = prefab.GetComponent<MeshRenderer>();
            var filter = prefab.GetComponent<MeshFilter>();

            var target = m_data.simulation.TryGetCharacter(projectile.targetCharacterID);
            if (target == null)
                continue;

            var time = projectile.time + projectile.speed * CB2.Simulation.DeltaTime * m_data.lerp;

            Vector3 pos = Vector3.Lerp(projectile.origin.ToUnity(), target.state.position.ToUnity(), time);
            float dist = (target.state.position - projectile.origin).magnitude;

            float arc = .1f * dist;
            pos.y += 1 + arc*Mathf.Sin(time * Mathf.PI);
            Quaternion rot = 
                Quaternion.LookRotation(
                target.state.position.ToUnity()
                - projectile.origin.ToUnity()
                + Vector3.up * 2*arc *Mathf.Cos(time * Mathf.PI)) 
                * Quaternion.Euler(0, 90, 0);

            Graphics.DrawMesh(filter.sharedMesh, pos, rot, renderer.sharedMaterial, 1, Camera.main);
        }

        for (int i = m_data.deaths.Count - 1; i >= 0; --i)
        {
            if (UpdateDeathAnimation(m_data.deaths[i]))
            {
                Destroy(m_data.deaths[i].go);
                m_data.deaths.RemoveAt(i);
            }
        }


       // m_spriteBatch.Draw(AlphaBlendShader, SelectionSprite, m_data.simulation.State.map.primaryPoint.ToUnity(), 0, new Color(.5f, 1, .1f), new Vector3(4, 4));

        var points = m_data.simulation.State.map.capturePoints;
        for (int i = 0; i < points.Length; ++i)
        {
            var point = points[i];
            if (!point.active)
                continue;

            float r = 2*point.def.radius;
            for (int j = 0; j < point.capture.Length; ++j)
            {
                float t = point.capture[j] / point.def.captureTime;
                if (t > 1.0f)
                    t = 1.0f;
                if (point.def.type == CB2.CapturePointType.Primary)
                    m_spriteBatch.Draw(AdditiveShader, CapturePointSprite, point.def.position.ToUnity(), 0, new Color(0, 1, 0, .5f), t * r * Vector2.one);
                else
                    m_spriteBatch.Draw(AdditiveShader, CapturePointSprite, point.def.position.ToUnity(), 0, new Color(1, 1, 0, .5f), t * r * Vector2.one);
            }
            m_spriteBatch.Draw(AdditiveShader, CapturePointSprite, point.def.position.ToUnity(), 0, new Color(1, 1, 1, .2f), (r + .1f) * Vector2.one);
        }
    }

    private void UpdateGraphics(CharacterGraphics graphics)
    {
        var character = graphics.character;

        if (m_data.wasUpdated)
        {
            graphics.lastState = graphics.currentState;
            graphics.currentState = character.state;
        }

        CB2.Character.State lerpState = character.state;

        lerpState.position = CB2.Vector3.Lerp(graphics.lastState.position, graphics.currentState.position, m_data.lerp);
        lerpState.balance = CB2.Vector3.Lerp(graphics.lastState.balance, graphics.currentState.balance, m_data.lerp);
        lerpState.velocity = CB2.Vector3.Lerp(graphics.lastState.velocity, graphics.currentState.velocity, m_data.lerp);
        lerpState.rotation = CB2.Math.Lerp(graphics.lastState.rotation, graphics.currentState.rotation, m_data.lerp);

        if(graphics.currentState.attackTimer < graphics.lastState.attackTimer)
            lerpState.attackTimer = CB2.Math.Lerp(graphics.lastState.attackTimer, graphics.currentState.attackTimer, m_data.lerp);

        graphics.lerpState = lerpState;

        graphics.go.transform.position = lerpState.position.ToUnity();

        var balance = (lerpState.balance - lerpState.position) / character.def.moveSpd;
        var bdirz = Mathf.Atan(balance.z) * .5f * Mathf.Rad2Deg;
        var bdirx = Mathf.Atan(balance.x) * .5f * Mathf.Rad2Deg;
        graphics.go.transform.rotation = Quaternion.Euler(bdirz, 0, -bdirx) * Quaternion.Euler(0, lerpState.rotation, 0);

        var anims = character.def.weapon.anims;

        AnimationState attack = graphics.anim[anims.attack];
        AnimationState idle = graphics.anim[anims.idle];
        AnimationState walk = graphics.anim[anims.walk];
        AnimationState run = graphics.anim[anims.run];
        AnimationState takeDamage = graphics.anim[anims.takedamage];
        var velocity = lerpState.velocity.magnitude;

        if (character.takeDamage > 0.0f)
        {
            float spd = 20.0f;
            LerpWeight(ref idle, 0.0f, spd);
            LerpWeight(ref walk, 0.0f, spd);
            LerpWeight(ref run, 0.0f, spd);
            LerpWeight(ref attack, 0.0f, spd);
            LerpWeight(ref takeDamage, 1.0f, spd);
            takeDamage.normalizedTime = 1.0f - character.takeDamage;
            takeDamage.speed = 0;
        }
        else if(lerpState.attackTimer > 0.0f)
        {
            LerpWeight(ref idle, 0.0f);
            LerpWeight(ref walk, 0.0f);
            LerpWeight(ref run, 0.0f);
            LerpWeight(ref takeDamage, 0.0f);
            LerpWeight(ref attack, 1.0f);
            attack.normalizedTime = 1.0f - lerpState.attackTimer;
            attack.speed = 0;
        }
        else if(velocity > 0.1f)
        {
            LerpWeight(ref attack, 0.0f);
            LerpWeight(ref idle, 0.0f);
            LerpWeight(ref takeDamage, 0.0f);

            // Negative speed when moving backwards
            float rot = (lerpState.rotation - 90) * Mathf.Deg2Rad;
            float animSpeed = Vector3.Dot(lerpState.velocity.ToUnity(), new Vector3(Mathf.Cos(rot), 0, -Mathf.Sin(rot)));

            walk.speed = anims.walkSpeed * animSpeed;
            walk.wrapMode = WrapMode.Loop;

            if (run != null)
            {
                run.speed = anims.runSpeed * animSpeed;
                run.wrapMode = WrapMode.Loop;
            }
            if (velocity > 2.0f)
            {
                LerpWeight(ref walk, 0.0f);
                LerpWeight(ref run, 1.0f);
            }
            else
            {
                LerpWeight(ref walk, 1.0f);
                LerpWeight(ref run, 0.0f);
            }
        }
        else
        {
            LerpWeight(ref idle, 1.0f);
            LerpWeight(ref walk, 0.0f);
            LerpWeight(ref run, 0.0f);
            LerpWeight(ref takeDamage, 0.0f);
            LerpWeight(ref attack, 0.0f);
        }

        DrawIndicators(graphics, lerpState);
    }

    void PostRender(Camera cam)
    {
        if (cam != Camera.main)
            return;

        m_spriteBatch.DrawSprites();
    }

    private void DrawIndicators(CharacterGraphics graphics, CB2.Character.State lerpState)
    {
        Vector3 pos = lerpState.position.ToUnity();

        float radius = 2 * graphics.character.def.radius;

        m_spriteBatch.Draw(ShadowShader, BlobShadowSprite, pos, 0, new Color(0, 0, 0, .3f), Vector3.one * radius);

        // Draw selection
        var units = m_data.controlGroups[m_data.activeGroup];
        if (units.Contains(graphics.character))
        {
            //var matrix = Matrix4x4.TRS(lerpState.position.ToUnity(), Quaternion.identity, m_selectionScale);
            //Graphics.DrawMesh(m_seletionMesh, matrix, m_selectionMaterial, 1, Camera.main);
            m_spriteBatch.Draw(AlphaBlendShader, SelectionSprite, pos, 0, new Color(1, 1, 1, 1.0f), 1.2f * radius * Vector2.one);
            if(graphics.character.moveToTarget)
                m_spriteBatch.Draw(AlphaBlendShader, CircleSprite, graphics.character.moveTarget.ToUnity(), 0, new Color(1, 1, 1, 1.0f), .5f * radius * Vector2.one);
        }
        else if (m_data.ourTeam == graphics.character.team)
            m_spriteBatch.Draw(AlphaBlendShader, SelectionSprite, pos, 0, new Color(.4f, 1.0f, .6f), radius * Vector2.one);
        else
            m_spriteBatch.Draw(AlphaBlendShader, SelectionSprite, pos, 0, new Color(1.0f, 0.2f, .1f), radius * Vector2.one);


        float hp = (float)graphics.character.health / graphics.character.def.maxHealth;
        if (hp < .9f)
        {
            //pos.y += .0f;
            m_spriteBatch.Draw(AlphaBlendShader, CircleSprite, pos, 0, new Color(1, 0, 0, 1.0f - hp), .8f * radius * Vector2.one);
        }

    }

    private bool UpdateDeathAnimation(CharacterGraphics graphics)
    {
        var anims = graphics.character.def.weapon.anims;
        var death = graphics.anim[anims.death];
        death.wrapMode = WrapMode.Once;

        if (!death.enabled)
        {
            graphics.anim.Play(anims.death);
            death.speed = 0;
        }

        death.time = Mathf.Min(graphics.deathTimer, death.length);

        if (graphics.deathTimer >= death.length)
        {
            float t = graphics.deathTimer - death.length;
            graphics.go.transform.position += Vector3.down * .2f * t * Time.deltaTime;
        }

        if(graphics.deathTimer > death.length + 3.0f)
            return true;

        graphics.deathTimer += Time.deltaTime;

        return false;
    }

    private void LerpWeight(ref AnimationState state, float weight, float spd = 5.0f)
    {
        if (state == null)
            return;
        state.weight += (weight - state.weight) * spd * Time.deltaTime;
        if (state.weight < 0.01f)
            state.enabled = false;
        else
            state.enabled = true;
    }

    private GameData m_data;
    private GameObject[] m_prefabs;

    private Mesh m_seletionMesh;
    private Material m_selectionMaterial;
    private Vector3 m_selectionScale;
    private SpriteBatch m_spriteBatch;
}

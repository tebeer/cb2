﻿using UnityEngine.Rendering;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBatch
{
    public Matrix4x4 Matrix = Matrix4x4.identity;

    public SpriteBatch(bool postRender)
    {
        if (postRender)
        {
            Camera.onPostRender -= PostRender;
            Camera.onPostRender += PostRender;
        }
    }

    ~SpriteBatch()
    {
        Destroy();
    }

    public void Destroy()
    {
        Camera.onPostRender -= PostRender;
    }

    void PostRender(Camera camera)
    {
        DrawSprites();
    }

    public void Draw(Shader shader, Sprite sprite, Vector3 position, float angle, Color color, Vector2 scale, int layerIndex = 0)
    {
        var texture = sprite.texture;

        if (layerIndex >= m_layers.Count)
            for (int i = m_layers.Count; i <= layerIndex; ++i)
                m_layers.Add(new Layer());

        var layer = m_layers[layerIndex];

        SpriteShader spriteShader;
        if (!layer.shaders.TryGetValue(shader, out spriteShader))
        {
            spriteShader = new SpriteShader();
            layer.shaders.Add(shader, spriteShader);
        }

        SpriteMesh spriteMesh;
        if (!spriteShader.meshes.TryGetValue(texture, out spriteMesh))
        {
            spriteMesh = new SpriteMesh(shader, texture);
            spriteShader.meshes.Add(texture, spriteMesh);
        }

        spriteMesh.Add(sprite, position, angle, color, scale);
    }

    private void UpdateMeshes()
    {
        foreach (var layer in m_layers)
            foreach (var shader in layer.shaders)
                foreach (var mesh in shader.Value.meshes)
                    mesh.Value.UpdateMesh(false);
    }

    public void AddToCommandBuffer(CommandBuffer cb)
    {
        UpdateMeshes();
        foreach (var layer in m_layers)
            foreach (var shader in layer.shaders)
                foreach (var mesh in shader.Value.meshes)
                    mesh.Value.AddToCommandBuffer(Matrix, cb);
    }

    public void DrawSprites()
    {
        UpdateMeshes();
        foreach (var layer in m_layers)
            foreach (var shader in layer.shaders)
                foreach (var mesh in shader.Value.meshes)
                    mesh.Value.Draw(Matrix);
    }

    public void Clear()
    {
        foreach (var layer in m_layers)
            foreach (var shader in layer.shaders)
                foreach (var mesh in shader.Value.meshes)
                    mesh.Value.Clear();
    }

    private List<Layer> m_layers = new List<Layer>();

    private class Layer
    {
        public Dictionary<Shader, SpriteShader> shaders = new Dictionary<Shader, SpriteShader>();
    }

    private class SpriteShader
    {
        public Dictionary<Texture2D, SpriteMesh> meshes = new Dictionary<Texture2D, SpriteMesh>();
    }
}

public class SpriteMesh
{
    public SpriteMesh(Shader shader, Texture2D texture)
    {
        m_mesh = new Mesh();
        m_mesh.bounds = new Bounds(Vector3.zero, Vector3.one * 100000);
        m_material = new Material(shader);
        m_material.mainTexture = texture;
    }

    public void UpdateMesh(bool vertexOnly)
    {
        m_mesh.SetVertices(m_vertices);
        if (vertexOnly)
            return;
        m_mesh.SetUVs(0, m_uvs);
        m_mesh.SetColors(m_colors);
        m_mesh.SetTriangles(m_indices, 0, false);
    }

    public void Clear()
    {
        m_mesh.Clear();
        m_vertices.Clear();
        m_uvs.Clear();
        m_colors.Clear();
        m_indices.Clear();
    }

    public void Add(Sprite spr, Vector3 position, float angleRad, Color color, Vector2 scale)
    {
        var texwidth = spr.texture.width;
        var texheight = spr.texture.height;
        var textureRect = spr.textureRect;
        var rect = spr.rect;
        var uvRect = new Rect(textureRect.xMin / texwidth, textureRect.yMin / texheight, textureRect.width / texwidth, textureRect.height / texheight);

        int v = m_vertices.Count;
        float width = 1;//textureRect.width;
        float height = 1;//textureRect.height;

        var pivot = spr.pivot;
        pivot -= spr.textureRectOffset;
        pivot.x /= textureRect.width;
        pivot.y /= textureRect.height;
        //pivot.x -= textureRect.xMin;
        //pivot.y -= textureRect.yMin;

        float cos = Mathf.Cos(angleRad);
        float sin = Mathf.Sin(angleRad);

        float x1 = (       - pivot.x) * cos * scale.x - ( height - pivot.y) * sin * scale.y;
        float x2 = ( width - pivot.x) * cos * scale.x - ( height - pivot.y) * sin * scale.y;
        float x3 = ( width - pivot.x) * cos * scale.x - (        - pivot.y) * sin * scale.y;
        float x4 = (       - pivot.x) * cos * scale.x - (        - pivot.y) * sin * scale.y;

        float y1 = (       - pivot.x) * sin * scale.x + ( height - pivot.y) * cos * scale.y;
        float y2 = ( width - pivot.x) * sin * scale.x + ( height - pivot.y) * cos * scale.y;
        float y3 = ( width - pivot.x) * sin * scale.x + (        - pivot.y) * cos * scale.y;
        float y4 = (       - pivot.x) * sin * scale.x + (        - pivot.y) * cos * scale.y;

        m_vertices.Add(position + new Vector3(x1, 0, y1));
        m_vertices.Add(position + new Vector3(x2, 0, y2));
        m_vertices.Add(position + new Vector3(x3, 0, y3));
        m_vertices.Add(position + new Vector3(x4, 0, y4));
        
        m_uvs.Add(new Vector2(uvRect.xMin, uvRect.yMax));
        m_uvs.Add(new Vector2(uvRect.xMax, uvRect.yMax));
        m_uvs.Add(new Vector2(uvRect.xMax, uvRect.yMin));
        m_uvs.Add(new Vector2(uvRect.xMin, uvRect.yMin));
        m_colors.Add(color);
        m_colors.Add(color);
        m_colors.Add(color);
        m_colors.Add(color);
        m_indices.Add(v + 0);
        m_indices.Add(v + 1);
        m_indices.Add(v + 2);
        m_indices.Add(v + 2);
        m_indices.Add(v + 3);
        m_indices.Add(v + 0);
    }

    public void Set(int index, Vector3 position)
    {
        //int v = index * 4;
        //m_vertices[v + 0] = position + new Vector3(-m_def.size, YPos, -m_def.size);
        //m_vertices[v + 1] = position + new Vector3(m_def.size, YPos, -m_def.size);
        //m_vertices[v + 2] = position + new Vector3(m_def.size, YPos, m_def.size);
        //m_vertices[v + 3] = position + new Vector3(-m_def.size, YPos, m_def.size);
    }

    public void AddToCommandBuffer(Matrix4x4 matrix, CommandBuffer cb)
    {
        cb.DrawMesh(m_mesh, matrix, m_material);
    }

    public void Draw(Matrix4x4 matrix)
    {
        m_material.SetPass(0);
        Graphics.DrawMeshNow(m_mesh, matrix);
    }

    private Mesh m_mesh;
    private Material m_material;

    private List<Vector3> m_vertices = new List<Vector3>();
    private List<Vector2> m_uvs = new List<Vector2>();
    private List<Color> m_colors = new List<Color>();
    private List<int> m_indices = new List<int>();
}

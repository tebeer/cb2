﻿
public static class CB2Extensions
{
    public static UnityEngine.Vector3 ToUnity(this CB2.Vector3 v)
    {
        return new UnityEngine.Vector3(v.x, v.y, v.z);
    }
    public static CB2.Vector3 ToCB(this UnityEngine.Vector3 v)
    {
        return new CB2.Vector3(v.x, v.y, v.z);
    }
}